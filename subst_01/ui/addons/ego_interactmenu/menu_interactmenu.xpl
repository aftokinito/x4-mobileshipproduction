﻿-- ffi setup
local ffi = require("ffi")
local C = ffi.C
ffi.cdef[[
	typedef uint64_t BuildTaskID;
	typedef uint64_t UniverseID;
	typedef struct {
		const char* macro;
		const char* ware;
		uint32_t amount;
		uint32_t capacity;
	} AmmoData;
	typedef struct {
		int x;
		int y;
	} Coord2D;
	typedef struct {
		float x;
		float y;
		float z;
	} Coord3D;
	typedef struct {
		size_t queueidx;
		const char* state;
		const char* statename;
		const char* orderdef;
		size_t actualparams;
		bool enabled;
		bool isinfinite;
		bool issyncpointreached;
		bool istemporder;
	} Order;
	typedef struct {
		const char* id;
		const char* name;
		const char* desc;
		uint32_t amount;
		uint32_t numtiers;
		bool canhire;
	} PeopleInfo;
	typedef struct {
		uint32_t id;
		const char* text;
		const char* type;
		bool ispossible;
		bool istobedisplayed;
	} UIAction;
	typedef struct {
		UniverseID component;
		const char* connection;
	} UIComponentSlot;
	typedef struct {
		const char* shape;
		const char* name;
		uint32_t requiredSkill;
		float radius;
		bool rollMembers;
		bool rollFormation;
		size_t maxShipsPerLine;
	} UIFormationInfo;
	typedef struct {
		const float x;
		const float y;
		const float z;
		const float yaw;
		const float pitch;
		const float roll;
	} UIPosRot;
	typedef struct {
		const char* wareid;
		int32_t amount;
	} YieldInfo;
	bool CanAcceptSubordinate(UniverseID commanderid, UniverseID potentialsubordinateid);
	bool CancelConstruction(UniverseID containerid, BuildTaskID id);
	bool CanCancelConstruction(UniverseID containerid, BuildTaskID id);
	const char* CanTeleportPlayerTo(UniverseID controllableid, bool allowcontrolling, bool force);
	uint32_t GetAllLaserTowers(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllMines(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllNavBeacons(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllResourceProbes(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllSatellites(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetCompSlotPlayerActions(UIAction* result, uint32_t resultlen, UIComponentSlot compslot);
	Coord2D GetCompSlotScreenPos(UIComponentSlot compslot);
	UniverseID GetContextByClass(UniverseID componentid, const char* classname, bool includeself);
	uint32_t GetFormationShapes(UIFormationInfo* result, uint32_t resultlen);
	uint32_t GetMineablesAtSectorPos(YieldInfo* result, uint32_t resultlen, UniverseID sectorid, Coord3D position);
	uint32_t GetMissionThreadSubMissions(MissionID* result, uint32_t resultlen, MissionID missionid);
	uint32_t GetNumAllLaserTowers(UniverseID defensibleid);
	uint32_t GetNumAllMines(UniverseID defensibleid);
	uint32_t GetNumAllRoles(void);
	uint32_t GetNumAllNavBeacons(UniverseID defensibleid);
	uint32_t GetNumAllResourceProbes(UniverseID defensibleid);
	uint32_t GetNumAllSatellites(UniverseID defensibleid);
	uint32_t GetNumCompSlotPlayerActions(UIComponentSlot compslot);
	uint32_t GetNumFormationShapes(void);
	uint32_t GetNumMineablesAtSectorPos(UniverseID sectorid, Coord3D position);
	uint32_t GetNumMissionThreadSubMissions(MissionID missionid);
	uint32_t GetNumVenturePlatformDocks(UniverseID ventureplatformid);
	uint32_t GetNumVenturePlatforms(UniverseID defensibleid);
	uint32_t GetNumWares(const char* tags, bool research, const char* licenceownerid, const char* exclusiontags);
	UniverseID GetPlayerContainerID(void);
	UniverseID GetPlayerID(void);
	UniverseID GetPlayerOccupiedShipID(void);
	uint32_t GetPeople(PeopleInfo* result, uint32_t resultlen, UniverseID controllableid);
	float GetTextWidth(const char*const text, const char*const fontname, const float fontsize);
	uint32_t GetVenturePlatformDocks(UniverseID* result, uint32_t resultlen, UniverseID ventureplatformid);
	uint32_t GetVenturePlatforms(UniverseID* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetWares(const char** result, uint32_t resultlen, const char* tags, bool research, const char* licenceownerid, const char* exclusiontags);
	bool HasVenturerDock(UniverseID containerid, UniverseID shipid, UniverseID ventureplatformid);
	bool IsBuilderBusy(UniverseID shipid);
	bool IsDefensibleBeingBoardedBy(UniverseID defensibleid, const char* factionid);
	bool IsMouseEmulationActive(void);
	bool IsObjectKnown(const UniverseID componentid);
	bool IsPlayerCameraTargetViewPossible(UniverseID targetid, bool force);
	void LaunchLaserTower(UniverseID defensibleid, const char* lasertowermacroname);
	void LaunchMine(UniverseID defensibleid, const char* minemacroname);
	void LaunchNavBeacon(UniverseID defensibleid, const char* navbeaconmacroname);
	void LaunchResourceProbe(UniverseID defensibleid, const char* resourceprobemacroname);
	void LaunchSatellite(UniverseID defensibleid, const char* satellitemacroname);
	void MakePlayerOwnerOf(UniverseID objectid);
	void MovePlayerToSectorPos(UniverseID sectorid, UIPosRot position);
	void NotifyInteractMenuHidden(const uint32_t id, const bool allclosed);
	void NotifyInteractMenuShown(const uint32_t id);
	bool PerformCompSlotPlayerAction(UIComponentSlot compslot, uint32_t actionid);
	bool RemoveOrder(UniverseID controllableid, size_t idx, bool playercancelled, bool checkonly);
	bool RequestDockAt(UniverseID containerid, bool checkonly);
	UIFormationInfo SetFormationShape(UniverseID objectid, const char* formationshape);
	void SetGuidance(UniverseID componentid, UIPosRot offset);
	bool TeleportPlayerTo(UniverseID controllableid, bool allowcontrolling, bool instant, bool force);
]]

local utf8 = require("utf8")

local menu = {
	name = "InteractMenu",
	selectedRows = {},
	selectedplayerships = {},
}

local config = {
	layer = 3,
	width = 250,
	rowHeight = 16,
	entryFontSize = Helper.standardFontSize,
	entryX = 3,
	mouseOutRange = 100,
	border = 5,

	sections = {
		{ id = "main",					text = "",						isorder = false },
		{ id = "interaction",			text = ReadText(1001, 7865),	isorder = false },
		{ id = "hiringbuilderoption",	text = "",						isorder = false,	subsections = {
			{ id = "hiringbuilder",	text = ReadText(1001, 7873) },
		}},
		{ id = "trade",					text = ReadText(1001, 7104),	isorder = false },
		{ id = "playersquad_orders",	text = ReadText(1001, 1002),	isorder = false },	-- Broadcast
		{ id = "main_orders",			text = ReadText(1001, 7802),	isorder = false },
		{ id = "formationshapeoption",	text = "",						isorder = false,	subsections = {
			{ id = "formationshape",	text = ReadText(1001, 7862) },
		}},
		{ id = "main_assignments",		text = ReadText(1001, 7803),	isorder = false },
		{ id = "order",					text = "",						isorder = nil },
		{ id = "guidance",				text = "",						isorder = nil,		isplayerinteraction = true },
		{ id = "player_interaction",	text = ReadText(1001, 7843),	isorder = false,	isplayerinteraction = true },
		{ id = "consumables",			text = ReadText(1001, 7846),	isorder = false,	subsections = {
			{ id = "consumables_civilian",	text = ReadText(1001, 7847) },
			{ id = "consumables_military",	text = ReadText(1001, 7848) },
		}},
		{ id = "cheats",				text = "Cheats",				isorder = false }, -- (cheat only)
		{ id = "selected",				text = "",						isorder = true,		isplayerinteraction = true },
		{ id = "selected_orders_all",	text = ReadText(1001, 7804),	isorder = true },
		{ id = "selected_orders",		text = ReadText(1001, 7804),	isorder = true },
		{ id = "mining_orders",			text = "",						isorder = true,		subsections = {
			{ id = "mining",			text = ReadText(1041, 351) },
		}},
		{ id = "venturedockoption",		text = "",						isorder = true,		subsections = {
			{ id = "venturedock",	text = ReadText(1001, 7844) },
		}},
		{ id = "trade_orders",			text = ReadText(1001, 7861),	isorder = true },
		{ id = "selected_assignments",	text = ReadText(1001, 7805),	isorder = true },
		{ id = "selected_consumables",	text = ReadText(1001, 7849),	isorder = true,		subsections = {
			{ id = "selected_consumables_civilian",	text = ReadText(1001, 7847) },
			{ id = "selected_consumables_military",	text = ReadText(1001, 7848) },
		}},
	},
}

local function init()
	Menus = Menus or { }
	table.insert(Menus, menu)
	if Helper then
		Helper.registerMenu(menu)
	end

	RegisterEvent("hideInteractMenu", function () menu.onCloseElement("auto") end)
end

function menu.cleanup()
	menu.mode = nil
	menu.interactMenuID = nil
	menu.componentSlot = nil
	menu.connection = nil
	menu.componentOrder = nil
	menu.mission = nil
	menu.componentMissions = nil
	menu.missionoffer = nil
	menu.construction = nil
	menu.selectedplayerships = {}
	menu.selectednpcships = {}
	menu.playerSquad = {}
	menu.removedOccupiedPlayerShip = nil
	menu.showPlayerInteractions = nil
	menu.offsetcomponent = nil
	menu.offset = nil
	menu.mouseX = nil
	menu.mouseY = nil

	menu.subsection = nil
	menu.possibleorders = {}
	menu.numdockingpossible = nil
	menu.numremovableorders = nil
	menu.numwaitingforsignal = nil
	menu.actions = {}
	menu.holomapcolor = {}
	menu.texts = {}
	menu.colors = {}

	menu.contentTable = nil
	menu.currentOverTable = nil
	menu.selectedRows = {}
	menu.mouseOutBox = {}

	Helper.ffiClearNewHelper()
end

-- perform helpers

function menu.orderAttack(component, target, clear, immediate)
	if not C.IsOrderSelectableFor("Attack", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end
	local orderidx = C.CreateOrder(component, "Attack", false)
	SetOrderParam(component, orderidx, 1, nil, ConvertStringToLuaID(tostring(target)))
	C.EnableOrder(component, orderidx)
	if immediate then
		menu.setOrderImmediate(component, orderidx)
	end

	return orderidx
end

function menu.orderCollect(component, drop, sector, offset, clear)
	if not C.IsOrderSelectableFor("Collect", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end
	local orderidx = C.CreateOrder(component, "Collect", false)
	SetOrderParam(component, orderidx, 1, 0, ConvertStringToLuaID(tostring(drop)) )
	C.EnableOrder(component, orderidx)

	return orderidx
end

function menu.orderCollectRadius(component, sector, offset, clear)
	if not C.IsOrderSelectableFor("CollectDropsInRadius", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end
	local orderidx = C.CreateOrder(component, "CollectDropsInRadius", false)
	SetOrderParam(component, orderidx, 1, nil, { ConvertStringToLuaID(tostring(sector)), {offset.x, offset.y, offset.z} } )
	C.EnableOrder(component, orderidx)

	return orderidx
end

function menu.orderDeployAtPosition(component, sector, offset, macro, amount, clear)
	if not C.IsOrderSelectableFor("DeployObjectAtPosition", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end
	local orderidx = C.CreateOrder(component, "DeployObjectAtPosition", false)
	SetOrderParam(component, orderidx, 1, nil, { ConvertStringToLuaID(tostring(sector)), {offset.x, offset.y, offset.z} } )
	SetOrderParam(component, orderidx, 2, 0, macro )
	SetOrderParam(component, orderidx, 3, 0, amount )

	C.EnableOrder(component, orderidx)

	return orderidx
end

function menu.orderDeployToStation(component, station, clear)
	if not C.IsOrderSelectableFor("DeployToStation", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end
	local orderidx = C.CreateOrder(component, "DeployToStation", false)
	SetOrderParam(component, orderidx, 1, nil, ConvertStringToLuaID(tostring(station)))
	C.EnableOrder(component, orderidx)
end

function menu.orderDock(component, target, clear, ventureplatform)
	if not C.IsOrderSelectableFor("DockAndWait", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end
	local orderidx = C.CreateOrder(component, "DockAndWait", false)
	SetOrderParam(component, orderidx, 1, nil, ConvertStringToLuaID(tostring(target)))
	if ventureplatform then
		SetOrderParam(component, orderidx, 5, nil, ConvertStringToLuaID(tostring(ventureplatform)))
	end
	C.EnableOrder(component, orderidx)

	return orderidx
end

function menu.orderExplore(component, sectororgate, sector, offset, clear)
	if not C.IsOrderSelectableFor("Explore", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end
	local orderidx = C.CreateOrder(component, "Explore", false)
	SetOrderParam(component, orderidx, 1, nil, ConvertStringToLuaID(tostring(sectororgate)) )
	if C.IsComponentClass(sectororgate, "sector") or (C.IsComponentClass(sectororgate, "gate") and (not GetComponentData(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), "isactive"))) then
		SetOrderParam(component, orderidx, 2, nil, { ConvertStringToLuaID(tostring(sector)), {offset.x, offset.y, offset.z} } )
	end
	C.EnableOrder(component, orderidx)

	return orderidx
end

function menu.orderExploreUpdate(component, sectororgate, sector, offset, clear)
	if not C.IsOrderSelectableFor("ExploreUpdate", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end
	local orderidx = C.CreateOrder(component, "ExploreUpdate", false)
	SetOrderParam(component, orderidx, 1, nil, ConvertStringToLuaID(tostring(sectororgate)) )
	if C.IsComponentClass(sectororgate, "sector") or (C.IsComponentClass(sectororgate, "gate") and (not GetComponentData(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), "isactive"))) then
		SetOrderParam(component, orderidx, 2, nil, { ConvertStringToLuaID(tostring(sector)), {offset.x, offset.y, offset.z} } )
	end
	C.EnableOrder(component, orderidx)

	return orderidx
end

function menu.orderFollow(component, targetobject, clear)
	if not C.IsOrderSelectableFor("Follow", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end
	local orderidx = C.CreateOrder(component, "Follow", false)
	SetOrderParam(component, orderidx, 1, nil, ConvertStringToLuaID(tostring(targetobject)))
	C.EnableOrder(component, orderidx)

	return orderidx
end

function menu.orderMining(component, ware, sector, offset, clear)
	if not C.IsOrderSelectableFor("MiningPlayer", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end
	local orderidx
	if GetWareCapacity(component, ware, true) > 0 then
		orderidx = C.CreateOrder(component, "MiningPlayer", false)
		SetOrderParam(component, orderidx, 1, nil, { ConvertStringToLuaID(tostring(sector)), {offset.x, offset.y,offset.z} })
		SetOrderParam(component, orderidx, 3, nil, ware)
		C.EnableOrder(component, orderidx)
	end

	return orderidx
end

function menu.orderMoveWait(component, sector, offset, targetobject, clear)
	if not C.IsOrderSelectableFor("MoveWait", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end
	local orderidx
	if not C.IsComponentClass(targetobject, "sector") then
		orderidx = C.CreateOrder(component, "MoveToObject", false)
		SetOrderParam(component, orderidx, 1, nil, ConvertStringToLuaID(tostring(targetobject)))
	else
		orderidx = C.CreateOrder(component, "MoveWait", false)
		SetOrderParam(component, orderidx, 1, nil, { ConvertStringToLuaID(tostring(sector)), {offset.x, offset.y,offset.z} })
	end
	C.EnableOrder(component, orderidx)

	return orderidx
end

function menu.orderPlayerDockToTrade(component, target, clear)
	if not C.IsOrderSelectableFor("Player_DockToTrade", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end
	local orderidx = C.CreateOrder(component, "Player_DockToTrade", false)
	SetOrderParam(component, orderidx, 1, nil, ConvertStringToLuaID(tostring(target)))
	C.EnableOrder(component, orderidx)

	return orderidx
end

function menu.orderProtect(component, target, clear)
	if not C.IsOrderSelectableFor("ProtectStation", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end
	local orderidx = C.CreateOrder(component, "ProtectStation", false)
	SetOrderParam(component, orderidx, 1, nil, ConvertStringToLuaID(tostring(target)))
	C.EnableOrder(component, orderidx)

	return orderidx
end

function menu.orderRemove(ship)
	local numorders = C.GetNumOrders(ship)
	local currentorders = ffi.new("Order[?]", numorders)
	numorders = C.GetOrders(currentorders, numorders, ship)
	for i = numorders, 1, -1 do
		local isdocked, isdocking = GetComponentData(ConvertStringTo64Bit(tostring(ship)), "isdocked", "isdocking")
		if (i == 1) and ((ffi.string(currentorders[0].orderdef) == "DockAndWait") and (isdocked or isdocking)) then
			-- do nothing - removing the dock order would create an undock order ... rather have the ship stay put [Nick]
		else
			C.RemoveOrder(ship, i, false, false)
		end
	end
	local currentdefaultorder = ffi.new("Order")
	if C.GetDefaultOrder(currentdefaultorder, ship) then
		if ffi.string(currentdefaultorder.orderdef) ~= "Wait" and ffi.string(currentdefaultorder.orderdef) ~= "DockAndWait" and ffi.string(currentdefaultorder.orderdef) ~= "Escort" then
			C.CreateOrder(ship, "Wait", true)
			C.EnablePlannedDefaultOrder(ship, false)
		end
	end
end

function menu.orderStopAndHoldFire(component, clear, immediate)
	if not C.IsOrderSelectableFor("Wait", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end

	-- ship will stop and hold fire.
	-- params: 1: timeout, 2: allowdocked, 3: holdfire, 4: debugchance
	local orderidx = C.CreateOrder(component, "Wait", false)
	SetOrderParam(component, orderidx, 1, nil, clear and 0 or 3600)
	SetOrderParam(component, orderidx, 3, nil, true)
	C.EnableOrder(component, orderidx)
	if immediate then
		menu.setOrderImmediate(component, orderidx)
	end

	return orderidx
end

function menu.orderWithdrawAndHold(component, clear, immediate)
	if not C.IsOrderSelectableFor("MoveWait", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end

	local pos = ffi.new("UIPosRot")
	pos = C.GetObjectPositionInSector(component)
	local sectorid = GetComponentData(component, "sectorid")
	if sectorid then
		sectorid = ConvertIDTo64Bit(sectorid)
	end

	-- ship will hold fire and withdraw from combat, then hold position.
	-- params: 1: destination, 2: timeout, 3: withdraw, 4: debugchance
	local orderidx = C.CreateOrder(component, "MoveWait", false)
	-- HACK: MoveWait requires a destination but does not use it when $withdraw is true
	SetOrderParam(component, orderidx, 1, nil, { ConvertStringToLuaID(tostring(sectorid)), {pos.x, pos.y, pos.z} })
	SetOrderParam(component, orderidx, 2, nil, clear and 0 or 3600)
	SetOrderParam(component, orderidx, 3, nil, true)
	C.EnableOrder(component, orderidx)
	if immediate then
		menu.setOrderImmediate(component, orderidx)
	end

	return orderidx
end

function menu.orderWithdrawFromCombat(component, clear, immediate)
	if not C.IsOrderSelectableFor("Flee", component) then
		return
	end

	if clear then
		C.RemoveAllOrders(component)
	end

	-- ship will hold fire and withdraw from combat.
	-- params: 1: method ('boost','maneuver','highway','dock'), 2: return, 3: donotdrop, 4: deploydistraction, 5: holdfire, 6: attacker, 7: maxboostdistance, 8: maxboostduration, 9: log, 10: debugchance
	local orderidx = C.CreateOrder(component, "Flee", false)
	SetOrderParam(component, orderidx, 1, nil, 'boost')
	SetOrderParam(component, orderidx, 3, nil, true)
	SetOrderParam(component, orderidx, 4, nil, true)
	C.EnableOrder(component, orderidx)
	if immediate then
		menu.setOrderImmediate(component, orderidx)
	end

	return orderidx
end

-- other helpers

function menu.plotCourse(object, offset)
	local convertedObject = ConvertStringToLuaID(tostring(object))
	if (object == C.GetPlayerControlledShipID()) then
		return -- no plot course to playership or when menu.mode is set
	end

	if IsSameComponent(GetActiveGuidanceMissionComponent(), convertedObject) then
		C.EndGuidance()
	else
		if offset == nil then
			offset = ffi.new("UIPosRot", 0)
		elseif C.IsComponentClass(object, "sector") then
			object = C.GetZoneAt(object, offset)
		end
		C.SetGuidance(object, offset)
	end
end

-- widget scripts

function menu.buttonAccountManagement()
	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		Helper.closeMenuAndOpenNewMenu(menu, "MapMenu", { 0, 0, true, nil, nil, "infomode", { "info", ConvertStringTo64Bit(tostring(menu.componentSlot.component)), { "Personnel", "Manager" } } }, true)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "info", { ConvertStringTo64Bit(tostring(menu.componentSlot.component)), { "Personnel", "Manager" } })
		menu.cleanup()
	end
end

function menu.buttonAssignCommander(assignment)
	local convertedComponent = ConvertStringTo64Bit(tostring(menu.componentSlot.component))
	local isplayerownedtarget = GetComponentData(convertedComponent, "isplayerowned")

	for _, ship in ipairs(menu.selectedplayerships) do
		if (convertedComponent ~= ship) and isplayerownedtarget then
			if C.IsComponentClass(menu.componentSlot.component, "controllable") then
				local skip = false
				if GetComponentData(ship, "primarypurpose") == "mine" then
					if assignment == "trade" then
						skip = true
					end
				else
					if assignment == "mining" then
						skip = true
					end
				end
				if not skip then
					C.SetCommander(ship, menu.componentSlot.component, assignment)

					local orderindex = C.CreateOrder(ship, "AssignCommander", false)
					-- commander
					SetOrderParam(ship, orderindex, 1, nil, ConvertStringToLuaID(tostring(menu.componentSlot.component)))
					-- assignment
					SetOrderParam(ship, orderindex, 2, nil, assignment)
					-- cancelorders
					SetOrderParam(ship, orderindex, 3, nil, true)
					-- response
					SetOrderParam(ship, orderindex, 4, nil, true)

					C.EnableOrder(ship, orderindex)
				end
			end
		end
	end

	if not menu.shown then
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "refresh")
		menu.cleanup()
	end
end

function menu.buttonAttack(clear)
	for _, ship in ipairs(menu.selectedplayerships) do
		menu.orderAttack(ship, menu.componentSlot.component, clear)
	end

	menu.onCloseElement("close")
end

function menu.buttonAttackMultiple(clear)
	if not menu.shown then
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "attackmultiple", { ConvertStringTo64Bit(tostring(menu.componentSlot.component)), clear })
		menu.cleanup()
	end
end

function menu.buttonBoard()
	local selectedplayerships = {table.unpack(menu.selectedplayerships)}
	if menu.removedOccupiedPlayerShip then
		table.insert(selectedplayerships, menu.removedOccupiedPlayerShip)
	end

	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		Helper.closeMenuAndOpenNewMenu(menu, "MapMenu", { 0, 0, true, nil, nil, "boardingcontext", {ConvertStringTo64Bit(tostring(menu.componentSlot.component)), selectedplayerships} }, true)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.interactMenuCallbacks.returnToMenu("boardingcontext", { ConvertStringTo64Bit(tostring(menu.componentSlot.component)), selectedplayerships } )
		Helper.resetInteractMenuCallbacks()
		menu.cleanup()
	end
end

function menu.buttonCancelConstruction()
	C.CancelConstruction(menu.componentSlot.component, menu.construction.id)
	menu.onCloseElement("close")
end

function menu.buttonChangeAssignment(assignment)
	C.SetAssignment(menu.componentSlot.component, assignment)
	if menu.shown then
		menu.onCloseElement("close")
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "refresh")
		menu.cleanup()
	end
end

function menu.buttonCollect()
	for _, ship in ipairs(menu.selectedplayerships) do
		menu.orderCollect(ship, menu.componentSlot.component, menu.offsetcomponent, menu.offset, clear)
	end

	menu.onCloseElement("close")
end

function menu.buttonCollectRadius()
	for _, ship in ipairs(menu.selectedplayerships) do
		menu.orderCollectRadius(ship, menu.offsetcomponent, menu.offset, clear)
	end

	menu.onCloseElement("close")
end

function menu.buttonComm()
	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		local convertedComponent = ConvertStringTo64Bit(tostring(menu.componentSlot.component))
		local entities = Helper.getSuitableControlEntities(convertedComponent, true)
		Helper.closeMenuForNewConversation(menu, "default", entities[1], convertedComponent, true)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "comm", ConvertStringTo64Bit(tostring(menu.componentSlot.component)))
		menu.cleanup()
	end
end

function menu.buttonDeploy(type, macro, amount)
	if type == "mine" then
		for i = 1, amount do
			C.LaunchMine(menu.componentSlot.component, macro)
		end
	elseif type == "navbeacon" then
		for i = 1, amount do
			C.LaunchNavBeacon(menu.componentSlot.component, macro)
		end
	elseif type == "satellite" then
		for i = 1, amount do
			C.LaunchSatellite(menu.componentSlot.component, macro)
		end
	elseif type == "lasertower" then
		for i = 1, amount do
			C.LaunchLaserTower(menu.componentSlot.component, macro)
		end
	elseif type == "resourceprobe" then
		for i = 1, amount do
			C.LaunchResourceProbe(menu.componentSlot.component, macro)
		end
	end

	menu.onCloseElement("close")
end

function menu.buttonDeployAtPosition(type, macro, amount)
	for _, ship in ipairs(menu.selectedplayerships) do
		menu.orderDeployAtPosition(ship, menu.offsetcomponent, menu.offset, macro, amount)
	end

	menu.onCloseElement("close")
end

function menu.buttonDeployToStation(selectedbuilder, clear, target)
	if not C.IsBuilderBusy(selectedbuilder) then
		local convertedBuilder = ConvertStringTo64Bit(tostring(selectedbuilder))
		if not GetComponentData(convertedBuilder, "isplayerowned") then
			local playermoney = GetPlayerMoney()
			local fee = tonumber(C.GetBuilderHiringFee())
			if playermoney >= fee then
				TransferPlayerMoneyTo(fee, convertedBuilder)
			else
				return
			end
		end

		local station = target or menu.componentSlot.component
		if C.IsComponentClass(menu.componentSlot.component, "buildstorage") then
			station = ConvertIDTo64Bit(GetComponentData(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), "basestation")) or 0
		end
		menu.orderDeployToStation(convertedBuilder, station, clear)
	end

	menu.onCloseElement("close")
end

function menu.buttonDock(clear, ventureplatform)
	local convertedComponent = ConvertStringTo64Bit(tostring(menu.componentSlot.component))
	local convertedVenturePlatform = ventureplatform and ConvertStringTo64Bit(tostring(ventureplatform))
	for _, ship in ipairs(menu.selectedplayerships) do
		if IsDockingPossible(ship, convertedComponent, convertedVenturePlatform) then
			menu.orderDock(ship, menu.componentSlot.component, clear, ventureplatform)
		end
	end

	menu.onCloseElement("close")
end

function menu.buttonDockAtPlayer(clear)
	local playercontainer = C.GetPlayerContainerID()
	local convertedComponent = ConvertStringTo64Bit(tostring(playercontainer))
	for _, ship in ipairs(menu.selectedplayerships) do
		if IsDockingPossible(ship, convertedComponent) then
			menu.orderDock(ship, playercontainer, clear)
		end
	end
	menu.orderDock(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), playercontainer, clear)

	menu.onCloseElement("close")
end

function menu.buttonDockRequest()
	C.RequestDockAt(menu.componentSlot.component, false)

	menu.onCloseElement("close")
end

function menu.buttonEndGuidance()
	C.EndGuidance()
	menu.onCloseElement("close")
end

function menu.buttonExplore(clear)
	for _, ship in ipairs(menu.selectedplayerships) do
		menu.orderExplore(ship, menu.componentSlot.component, menu.offsetcomponent, menu.offset, clear)
	end

	menu.onCloseElement("close")
end

function menu.buttonExploreUpdate(clear)
	for _, ship in ipairs(menu.selectedplayerships) do
		menu.orderExploreUpdate(ship, menu.componentSlot.component, menu.offsetcomponent, menu.offset, clear)
	end

	menu.onCloseElement("close")
end

function menu.buttonExternal()
	local playersector = C.GetContextByClass(C.GetPlayerID(), "sector", false)
	local targetsector = C.GetContextByClass(menu.componentSlot.component, "sector", false)
	if (menu.componentSlot.component ~= C.GetPlayerControlledShipID()) and (playersector == targetsector) then
		local success = C.SetSofttarget(menu.componentSlot.component, "")
		if success then
			PlaySound("ui_target_set")
			C.SetPlayerCameraTargetView(menu.componentSlot.component, true)
		else
			PlaySound("ui_target_set_fail")
		end
	else
		PlaySound("ui_target_set_fail")
	end

	menu.onCloseElement("close")
end

function menu.buttonFollow(clear)
	for _, ship in ipairs(menu.selectedplayerships) do
		menu.orderFollow(ship, menu.componentSlot.component, clear)
	end

	menu.onCloseElement("close")
end

function menu.buttonFormationShape(shape, subordinates)
	local info = C.SetFormationShape(menu.componentSlot.component, shape)
	shape = ffi.string(info.shape)

	if (shape ~= "") then
		for i = #subordinates, 1, -1 do
			local subordinate = ConvertIDTo64Bit(subordinates[i])

			local numorders = C.GetNumOrders(subordinate)
			local currentorders = ffi.new("Order[?]", numorders)
			numorders = C.GetOrders(currentorders, numorders, subordinate)
			for j = 1, numorders do
				if ffi.string(currentorders[0].orderdef) == "Escort" then
					SetOrderParam(subordinate, j, 2, nil, shape) -- shape
					SetOrderParam(subordinate, j, 3, nil, info.radius) -- radius
					SetOrderParam(subordinate, j, 4, nil, info.rollMembers) -- rollmembers
					SetOrderParam(subordinate, j, 5, nil, info.rollFormation) -- rollformation
					SetOrderParam(subordinate, j, 6, nil, tonumber(info.maxShipsPerLine)) -- maxshipsperline
				end
			end

			local currentdefaultorder = ffi.new("Order")
			if C.GetDefaultOrder(currentdefaultorder, subordinate) then
				if ffi.string(currentdefaultorder.orderdef) == "Escort" then
					SetOrderParam(subordinate, "default", 2, nil, shape) -- shape
					SetOrderParam(subordinate, "default", 3, nil, info.radius) -- radius
					SetOrderParam(subordinate, "default", 4, nil, info.rollMembers) -- rollmembers
					SetOrderParam(subordinate, "default", 5, nil, info.rollFormation) -- rollformation
					SetOrderParam(subordinate, "default", 6, nil, tonumber(info.maxShipsPerLine)) -- maxshipsperline
				end
			end
		end
	end

	menu.onCloseElement("close")
end

function menu.buttonGuidance(useoffset)
	if useoffset then 
		menu.plotCourse(menu.offsetcomponent, menu.offset)
	else 
		menu.plotCourse(menu.componentSlot.component, nil)
	end 

	menu.onCloseElement("close")
end

function menu.buttonInfo()
	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		Helper.closeMenuAndOpenNewMenu(menu, "MapMenu", { 0, 0, true, nil, nil, "infomode", { "info", ConvertStringTo64Bit(tostring(menu.componentSlot.component)) } }, true)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "info", { ConvertStringTo64Bit(tostring(menu.componentSlot.component)) })
		menu.cleanup()
	end
end

function menu.buttonMining(ware, clear)
	for _, ship in ipairs(menu.selectedplayerships) do
		menu.orderMining(ship, ware, menu.offsetcomponent, menu.offset, clear)
	end

	menu.onCloseElement("close")
end

function menu.buttonMissionSetInactive()
	C.SetActiveMission(0)

	if not menu.shown then
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "refresh")
		menu.cleanup()
	else
		menu.onCloseElement("close")
	end
end

function menu.buttonMissionSetActive(missionid)
	C.SetActiveMission(missionid)

	if not menu.shown then
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "refresh")
		menu.cleanup()
	else
		menu.onCloseElement("close")
	end
end

function menu.buttonMissionAbort(missionid)
	C.AbortMission(missionid)

	if not menu.shown then
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "refresh")
		menu.cleanup()
	else
		menu.onCloseElement("close")
	end
end

function menu.buttonMissionShow(missionid)
	local missiondetails = C.GetMissionIDDetails(missionid)
	local entry = {
		["missionGroup"] = {},
		["maintype"] = ffi.string(missiondetails.mainType),
	}
	if missiondetails.threadMissionID ~= 0 then
		local missionGroup = C.GetMissionGroupDetails(missiondetails.threadMissionID)
		entry.missionGroup.id = ffi.string(missionGroup.id)
		entry.missionGroup.name = ffi.string(missionGroup.name)
	else
		local missionGroup = C.GetMissionGroupDetails(missionid)
		entry.missionGroup.id = ffi.string(missionGroup.id)
		entry.missionGroup.name = ffi.string(missionGroup.name)
	end
	
	local mode
	if entry.maintype == "upkeep" then
		mode = "upkeep"
	else
		if entry.maintype == "guidance" then
			DebugError("menu.buttonMissionShow(): Trying to show guidance mission. [Florian]")
		else
			if entry.missionGroup.id ~= "" then
				mode = "guild"
			else
				mode = "other"
			end
		end
	end

	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		Helper.closeMenuAndOpenNewMenu(menu, "MapMenu", { 0, 0, true, nil, nil, "infomode", { "mission", mode, ConvertStringTo64Bit(tostring(missionid)) } }, true)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "mission", { mode, ConvertStringTo64Bit(tostring(missionid)) })
		menu.cleanup()
	end
end

function menu.buttonMissionBriefing(missionid, isoffer)
	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		Helper.closeMenuAndOpenNewMenu(menu, "MissionBriefingMenu", { 0, 0, ConvertStringToLuaID(tostring(missionid)), isoffer }, true)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "newmenu", { "MissionBriefingMenu", { 0, 0, ConvertStringToLuaID(tostring(missionid)), isoffer } })
		menu.cleanup()
	end
end

function menu.buttonMissionAccept(offerid)
	local name, description, difficulty, threadtype, maintype, subtype, subtypename, faction, rewardmoney, rewardtext, briefingobjectives, activebriefingstep, briefingmissions, oppfaction, licence, missiontime, duration, abortable, guidancedisabled, associatedcomponent, alertLevel, offeractor, offercomponent = GetMissionOfferDetails(ConvertStringToLuaID(menu.missionoffer))

	SignalObject(offeractor, "accept", ConvertStringToLuaID(tostring(offerid)))

	if not menu.shown then
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "missionaccepted", { offerid })
		menu.cleanup()
	else
		menu.onCloseElement("close")
	end
end

function menu.buttonMoveWait(clear)
	for _, ship in ipairs(menu.selectedplayerships) do
		menu.orderMoveWait(ship, menu.offsetcomponent, menu.offset, menu.componentSlot.component, clear)
	end

	menu.onCloseElement("close")
end

function menu.buttonPlayerSquadAttackPlayerTarget(clear)
	local playertarget = ConvertIDTo64Bit(GetPlayerTarget())
	if (playertarget ~= 0) then
		for ship, _ in pairs(menu.playerSquad) do
			menu.orderAttack(ship, playertarget, clear, true)
			--print("ordering " .. ffi.string(C.GetComponentName(ship)) .. " " .. tostring(ship) .. " to attack " .. ffi.string(C.GetComponentName(playertarget)) .. " " .. tostring(playertarget))
		end
	end

	menu.onCloseElement("close")
end

function menu.buttonPlayerSquadStopAndHoldFire(clear)
	for ship, _ in pairs(menu.playerSquad) do
		menu.orderStopAndHoldFire(ship, clear, true)
		--print("ordering " .. ffi.string(C.GetComponentName(ship)) .. " " .. tostring(ship) .. " to halt and stop firing.")
	end

	menu.onCloseElement("close")
end

function menu.buttonPlayerSquadWithdrawAndHold(clear)
	for ship, _ in pairs(menu.playerSquad) do
		menu.orderWithdrawAndHold(ship, clear, true)
		--print("ordering " .. ffi.string(C.GetComponentName(ship)) .. " " .. tostring(ship) .. " to withdraw from combat and hold position.")
	end

	menu.onCloseElement("close")
end

function menu.buttonPlayerSquadWithdrawFromCombat(clear)
	for ship, _ in pairs(menu.playerSquad) do
		menu.orderWithdrawFromCombat(ship, clear, true)
		--print("ordering " .. ffi.string(C.GetComponentName(ship)) .. " " .. tostring(ship) .. " to withdraw from combat.")
	end

	menu.onCloseElement("close")
end

function menu.buttonPerformPlayerAction(id, type)
	C.PerformCompSlotPlayerAction(menu.componentSlot, id)
	AddUITriggeredEvent(menu.name, "perform", type)
	menu.onCloseElement("close", false)
end

function menu.buttonPlayerDockToTrade(clear)
	local convertedComponent = ConvertStringTo64Bit(tostring(menu.componentSlot.component))
	for _, ship in ipairs(menu.selectedplayerships) do
		if IsDockingPossible(ship, convertedComponent) then
			menu.orderPlayerDockToTrade(ship, menu.componentSlot.component, clear)
		end
	end

	menu.onCloseElement("close")
end

function menu.buttonProceedWithOrders()
	SignalObject(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), "playerownedship_proceed")
	for _, ship in ipairs(menu.selectedplayerships) do
		SignalObject(ship, "playerownedship_proceed")
	end
	menu.onCloseElement("close")
end

function menu.buttonProtect(clear)
	for _, ship in ipairs(menu.selectedplayerships) do
		menu.orderProtect(ship, menu.componentSlot.component, clear)
	end

	menu.onCloseElement("close")
end

function menu.buttonRecallSubordinates(component, subordinates, level)
	if not level then
		level = 1
	end

	if not component then
		DebugError("menu.buttonRecallSubordinates(): " .. tostring(component) .. " is not a valid component.")
	elseif not subordinates or #subordinates < 1 then
		DebugError("menu.buttonRecallSubordinates(): tried to recall subordinates on " .. tostring(component) .. " that has no subordinates.")
	else
		for i = #subordinates, 1, -1 do
			local subordinate = ConvertIDTo64Bit(subordinates[i])
			local numorders = C.GetNumOrders(subordinate)
			for j = numorders, 1, -1 do
				C.RemoveOrder(subordinate, j, false, false)
			end

			local subsubordinates = GetSubordinates(subordinates[i])
			if #subsubordinates > 0 then
				menu.buttonRecallSubordinates(subordinate, subsubordinates, level + 1)
			end
		end
	end

	if level == 1 then
		menu.onCloseElement("close")
	end
end

function menu.buttonRemoveAllOrders()
	menu.orderRemove(menu.componentSlot.component)
	for _, ship in ipairs(menu.selectedplayerships) do
		menu.orderRemove(ship)
	end
	menu.onCloseElement("close")
end

function menu.buttonRemoveAssignment()
	C.RemoveCommander(menu.componentSlot.component)
	C.CreateOrder(menu.componentSlot.component, "Wait", true)
	C.EnablePlannedDefaultOrder(menu.componentSlot.component, false)
	if menu.shown then
		menu.onCloseElement("close")
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "refresh")
		menu.cleanup()
	end
end

function menu.buttonRemoveOrder()
	C.RemoveOrder(menu.componentSlot.component, menu.componentOrder.queueidx, false, false)
	menu.onCloseElement("close")
end

function menu.buttonUpgrade(clear)
	-- local shiptrader = GetComponentData(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), "shiptrader")
	-- if shiptrader then
	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		Helper.closeMenuAndOpenNewMenu(menu, "ShipConfigurationMenu", { 0, 0, ConvertStringToLuaID(tostring(menu.componentSlot.component)), "upgrade", menu.selectedplayerships }, true)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "newmenu", { "ShipConfigurationMenu", { 0, 0, ConvertStringToLuaID(tostring(menu.componentSlot.component)), "upgrade", menu.selectedplayerships } })
		menu.cleanup()
	end
	-- else
		-- DebugError("menu.buttonUpgrade(): unable to retrieve ship trader of " .. ffi.string(C.GetComponentName(menu.componentSlot.component)) .. ".")
	-- end
end

function menu.buttonPaintMod()
	local found = false
	local ships = {}
	if menu.shown then
		table.insert(ships, menu.componentSlot.component)
	else
		for _, ship in ipairs(menu.selectedplayerships) do
			table.insert(ships, ship)
			if ship == menu.componentSlot.component then
				found = true
			end
		end
		if not found then
			table.insert(ships, menu.componentSlot.component)
		end
	end

	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		Helper.closeMenuAndOpenNewMenu(menu, "ShipConfigurationMenu", { 0, 0, nil, "modify", { true, ships } }, true)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "newmenu", { "ShipConfigurationMenu", { 0, 0, nil, "modify", { true, ships } } })
		menu.cleanup()
	end
end

function menu.buttonSellShips()
	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		Helper.closeMenuAndOpenNewMenu(menu, "MapMenu", { 0, 0, true, nil, nil, "sellships", { ConvertStringToLuaID(tostring(menu.componentSlot.component)), menu.selectedplayerships, menu.frameX, menu.frameY } }, true)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "sellships", { ConvertStringTo64Bit(tostring(menu.componentSlot.component)), menu.selectedplayerships, menu.frameX, menu.frameY })
		menu.cleanup()
	end
end

function menu.buttonShipConfig(mode)
	-- local shiptrader = GetComponentData(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), "shiptrader")
	-- if shiptrader then
		if menu.shown then
			if menu.interactMenuID then
				C.NotifyInteractMenuHidden(menu.interactMenuID, true)
			end
			Helper.closeMenuAndOpenNewMenu(menu, "ShipConfigurationMenu", { 0, 0, menu.componentSlot.component, mode }, true)
			menu.cleanup()
		else
			Helper.resetUpdateHandler()
			Helper.clearFrame(menu, config.layer)
			Helper.returnFromInteractMenu(menu.currentOverTable, "newmenu", { "ShipConfigurationMenu", { 0, 0, menu.componentSlot.component, mode } })
			menu.cleanup()
		end
	-- else
		-- DebugError("menu.buttonShipConfig(): unable to retrieve ship trader of " .. ffi.string(C.GetComponentName(menu.componentSlot.component)) .. ".")
	-- end
end

function menu.buttonStandingOrders()
	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		Helper.closeMenuAndOpenNewMenu(menu, "MapMenu", { 0, 0, true, nil, nil, "infomode", { "controllableresponses", ConvertStringTo64Bit(tostring(menu.componentSlot.component)) } }, true)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "standingorders", ConvertStringTo64Bit(tostring(menu.componentSlot.component)))
		menu.cleanup()
	end
end

function menu.buttonStationConfig()
	local station = menu.componentSlot.component
	if C.IsComponentClass(menu.componentSlot.component, "buildstorage") then
		station = ConvertIDTo64Bit(GetComponentData(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), "basestation")) or 0
	end

	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		Helper.closeMenuAndOpenNewMenu(menu, "StationConfigurationMenu", { 0, 0, station }, true)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "newmenu", { "StationConfigurationMenu", { 0, 0, station } })
		menu.cleanup()
	end
end

function menu.buttonStationOverview()
	local station = menu.componentSlot.component
	if C.IsComponentClass(menu.componentSlot.component, "buildstorage") then
		station = ConvertIDTo64Bit(GetComponentData(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), "basestation")) or 0
	end

	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		Helper.closeMenuAndOpenNewMenu(menu, "StationOverviewMenu", { 0, 0, station }, true)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "newmenu", { "StationOverviewMenu", { 0, 0, station } })
		menu.cleanup()
	end
end

function menu.buttonTeleport()
	local isplayerownedtarget = GetComponentData(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), "isplayerowned")

	C.TeleportPlayerTo(menu.componentSlot.component, false, menu.mode == "shipconsole", (menu.mode == "shipconsole") and isplayerownedtarget)
	menu.onCloseElement("close")
end

function menu.buttonTrade(wareexchange, tradepartner)
	tradepartner = tradepartner or ConvertStringTo64Bit(tostring(menu.componentSlot.component))

	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		Helper.closeMenuAndOpenNewMenu(menu, "MapMenu", { 0, 0, true, nil, nil, 'tradecontext', { tradepartner, nil, wareexchange } }, true)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "tradecontext", { tradepartner, nil, wareexchange } )
		menu.cleanup()
	end
end

-- cheats

function menu.buttonOwnerCheat()
	C.MakePlayerOwnerOf(menu.componentSlot.component)
	C.CreateOrder(menu.componentSlot.component, "Wait", true)
	C.EnablePlannedDefaultOrder(menu.componentSlot.component, false)

	if menu.shown then
		menu.onCloseElement("close")
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.returnFromInteractMenu(menu.currentOverTable, "refresh")
		menu.cleanup()
	end
end

function menu.buttonSatelliteCheat()
	local macro = GetWareData("satellite_mk2", "component")
	C.SpawnObjectAtPos(macro, menu.offsetcomponent, menu.offset)
	menu.onCloseElement("close")
end

function menu.buttonNavBeaconCheat()
	local macro = GetWareData("waypointmarker_01", "component")
	C.SpawnObjectAtPos(macro, menu.offsetcomponent, menu.offset)
	menu.onCloseElement("close")
end

function menu.buttonResourceProbeCheat()
	local macro = GetWareData("resourceprobe_01", "component")
	C.SpawnObjectAtPos(macro, menu.offsetcomponent, menu.offset)
	menu.onCloseElement("close")
end

function menu.buttonWarpCheat()
	C.MovePlayerToSectorPos(menu.offsetcomponent, menu.offset)
	menu.onCloseElement("close")
end

-- interact menu hooks

function menu.showInteractMenu(param)
	AddUITriggeredEvent(menu.name, "", menu.param)
	menu.componentSlot = ffi.new("UIComponentSlot")
	menu.componentSlot.component = ConvertStringTo64Bit(tostring(param.component))
	local connection = "connectionui"
	menu.connection = Helper.ffiNewString(connection)	-- store string here to keep it alive during lifetime of menu.componentSlot
	menu.componentSlot.connection = menu.connection
	menu.componentOrder = param.order
	menu.mission = param.mission
	menu.componentMissions = param.componentmissions
	menu.missionoffer = param.missionoffer
	menu.construction = param.construction
	menu.selectedplayerships = param.playerships
	menu.selectednpcships = param.npcships
	menu.playerSquad = menu.getPlayerSquad()
	menu.offsetcomponent = param.offsetcomponent
	menu.offset = param.offset
	menu.mouseX = param.mouseX
	menu.mouseY = param.mouseY

	menu.processSelectedPlayerShips()

	menu.display()
end

function menu.onShowMenu(_, _, serializedArg)
	if menu.param and (menu.param[3] == "shipconsole") then
		menu.mode = "shipconsole"
		menu.interactMenuID = nil
		menu.componentSlot = ffi.new("UIComponentSlot")
		menu.componentSlot.component = ConvertIDTo64Bit(menu.param[4])
		local connection = "connectionui"
		menu.connection = Helper.ffiNewString(connection)	-- store string here to keep it alive during lifetime of menu.componentSlot
		menu.componentSlot.connection = menu.connection
	else
		menu.interactMenuID = tonumber(string.match(serializedArg, "%d+"))
		serializedArg = string.sub(serializedArg, string.find(serializedArg, ";") + 1)
		menu.componentSlot = ffi.new("UIComponentSlot")
		menu.componentSlot.component = ConvertStringTo64Bit(string.match(serializedArg, "%d+"))
		local connection = string.sub(serializedArg, string.find(serializedArg, ";") + 1)
		menu.connection = Helper.ffiNewString(connection)	-- store string here to keep it alive during lifetime of menu.componentSlot
		menu.componentSlot.connection = menu.connection
	end

	local occupiedPlayerShip = C.GetPlayerOccupiedShipID()
	if occupiedPlayerShip ~= 0 then
		menu.selectedplayerships = { ConvertStringTo64Bit(tostring(occupiedPlayerShip)) }
	else
		menu.selectedplayerships = {}
	end
	menu.selectednpcships = {}
	menu.processSelectedPlayerShips()
	menu.playerSquad = menu.getPlayerSquad()

	if menu.interactMenuID then
		C.NotifyInteractMenuShown(menu.interactMenuID)
	end

	menu.display()
end

function menu.onShowMenuSound()
	-- no sound
end

-- displaying the menu

function menu.display()
	menu.width = Helper.scaleX(config.width)

	local posX, posY = GetLocalMousePosition()
	if menu.mode == "shipconsole" then
		posX, posY = -menu.width / 2, 0
	end
	if posX == nil then
		-- gamepad case
		if menu.mouseX ~= nil then
			posX = menu.mouseX + Helper.viewWidth / 2
			posY = menu.mouseY + Helper.viewHeight / 2
		else
			local pos = C.GetCompSlotScreenPos(menu.componentSlot)
			posX = pos.x + Helper.viewWidth / 2
			posY = pos.y + Helper.viewHeight / 2
		end
	else
		posX = posX + Helper.viewWidth / 2
		posY = Helper.viewHeight / 2 - posY
	end
	menu.posX = posX
	menu.posY = posY

	menu.getHolomapColors()
	menu.prepareTexts()
	menu.prepareActions()

	menu.draw()
end

function menu.draw()
	local width = menu.width
	if menu.subsection then
		width = 2 * width + Helper.borderSize
	end
	
	menu.frameX = math.max(0, menu.posX)
	menu.frameY = math.max(0, menu.posY)
	local frame = Helper.createFrameHandle(menu, {
		x = menu.frameX,
		y = 0,
		width = width + Helper.scrollbarWidth,
		layer = config.layer,
		standardButtons = { close = true },
		standardButtonX = Helper.scrollbarWidth,
		autoFrameHeight = true,
		closeOnUnhandledClick = true,
		playerControls = true,
		startAnimation = false,
	})

	local content_position = "left"
	local subsection_position = "right"
	if menu.subsection then
		if frame.properties.x + menu.width > Helper.viewWidth then
			-- content table is at the right border -> subsection table goes to the left
			menu.frameX = Helper.viewWidth - width - config.border
			content_position = "right"
			subsection_position = "left"
		elseif frame.properties.x + width > Helper.viewWidth then
			-- not enough space for the subsection table -> subsection table goes to the left
			menu.frameX = frame.properties.x - menu.width - Helper.borderSize
			content_position = "right"
			subsection_position = "left"
		else
			frame.properties.standardButtonX = frame.properties.standardButtonX + menu.width + Helper.borderSize
		end
	else
		if frame.properties.x + menu.width > Helper.viewWidth then
			menu.frameX = Helper.viewWidth - menu.width - config.border
		end
	end
	frame.properties.x = menu.frameX

	Helper.removeAllWidgetScripts(menu, config.layer)
	local contentTable = menu.createContentTable(frame, content_position)
	local subsectionTable
	if menu.subsection then
		subsectionTable = menu.createSubSectionTable(frame, subsection_position)
	end

	local height = contentTable:getFullHeight()
	local subsectionHeight = 0
	if menu.frameY + height > Helper.viewHeight then
		menu.frameY = Helper.viewHeight - height - config.border
	else
		if menu.mode == "shipconsole" then
			menu.frameY = Helper.viewHeight / 2 - height / 2
		end
	end
	frame.properties.y = menu.frameY
	if menu.subsection then
		subsectionHeight = subsectionTable:getFullHeight()
		if frame.properties.y + menu.subsection.y + subsectionHeight > Helper.viewHeight then
			subsectionTable.properties.y = menu.subsection.y - subsectionHeight + (config.rowHeight + Helper.borderSize)
			if subsectionHeight > height then
				local diff = subsectionHeight - height
				menu.frameY = frame.properties.y - diff
				frame.properties.y = menu.frameY
				frame.properties.standardButtonY = diff
				contentTable.properties.y = diff
				subsectionTable.properties.y = 0
			end
		else
			subsectionTable.properties.y = menu.subsection.y
		end
	end


	menu.mouseOutBox = {
		x1 =   frame.properties.x -  Helper.viewWidth / 2                                      - config.mouseOutRange,
		x2 =   frame.properties.x -  Helper.viewWidth / 2 + width                              + config.mouseOutRange,
		y1 = - frame.properties.y + Helper.viewHeight / 2                                      + config.mouseOutRange,
		y2 = - frame.properties.y + Helper.viewHeight / 2 - frame:getUsedHeight() - config.mouseOutRange
	}
	
	frame:display()
end

function menu.addSectionTitle(ftable, section, first)
	local height = 0
	if section.text ~= "" then
		if not first then
			local row = ftable:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
			row[1]:setColSpan(2):createText("", { minRowHeight = config.rowHeight / 2, fontsize = 1 })
			height = height + row:getHeight() + Helper.borderSize
		end
		local row = ftable:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
		if section.id == "main_assignments" then
			row[1]:createText(string.format(section.text, menu.texts.commanderShortName), { font = Helper.standardFontBold, mouseOverText = menu.texts.commanderName, titleColor = Helper.defaultSimpleBackgroundColor })
			row[2]:createText("[" .. GetComponentData(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), "assignmentname") .. "]", { font = Helper.standardFontBold, halign = "right", height = Helper.subHeaderHeight, titleColor = Helper.defaultSimpleBackgroundColor })
		elseif section.id == "player_interaction" then
			if not first then
				row[1]:createText(section.text, { font = Helper.standardFontBold, mouseOverText = section.text .. " " .. menu.texts.targetName, titleColor = Helper.defaultSimpleBackgroundColor })
				row[2]:createText(menu.texts.targetBaseName or menu.texts.targetShortName, { font = Helper.standardFontBold, halign = "right", color = menu.colors.target, mouseOverText = section.text .. " " .. menu.texts.targetName, height = Helper.subHeaderHeight, titleColor = Helper.defaultSimpleBackgroundColor })
			end
		elseif (section.id == "selected_orders_all") then
			row[1]:createText(section.text, { font = Helper.standardFontBold, mouseOverText = menu.texts.selectedFullNamesAll, titleColor = Helper.defaultSimpleBackgroundColor })
			row[2]:createText(menu.texts.selectedNameAll, { font = Helper.standardFontBold, halign = "right", color = menu.colors.selected, mouseOverText = menu.texts.selectedFullNamesAll, height = Helper.subHeaderHeight, titleColor = Helper.defaultSimpleBackgroundColor })
		elseif (section.id == "selected_orders") or (section.id == "trade_orders") or (section.id == "selected_assignments") then
			row[1]:createText(section.text, { font = Helper.standardFontBold, mouseOverText = menu.texts.selectedFullNames, titleColor = Helper.defaultSimpleBackgroundColor })
			row[2]:createText(menu.texts.selectedName, { font = Helper.standardFontBold, halign = "right", color = menu.colors.selected, mouseOverText = menu.texts.selectedFullNames, height = Helper.subHeaderHeight, titleColor = Helper.defaultSimpleBackgroundColor })
		else
			row[1]:setColSpan(2):createText(section.text, { font = Helper.standardFontBold, height = Helper.subHeaderHeight, titleColor = Helper.defaultSimpleBackgroundColor })
		end
		height = height + row:getHeight() + Helper.borderSize
	end
	return height
end

function menu.createContentTable(frame, position)
	local x = 0
	if position == "right" then
		x = menu.width + Helper.borderSize
	end

	local ftable = frame:addTable(2, { tabOrder = menu.subsection and 2 or 1, x = x, width = menu.width, backgroundID = "solid", backgroundColor = Helper.color.semitransparent, highlightMode = "off" })
	ftable:setDefaultCellProperties("text",   { minRowHeight = config.rowHeight, fontsize = config.entryFontSize, x = config.entryX })
	ftable:setDefaultCellProperties("button", { height = config.rowHeight })
	ftable:setDefaultComplexCellProperties("button", "text", { fontsize = config.entryFontSize, x = config.entryX })
	ftable:setDefaultComplexCellProperties("button", "text2", { fontsize = config.entryFontSize, x = config.entryX })
	ftable:setColWidthPercent(2, 40)
	ftable:setDefaultBackgroundColSpan(1, 2)

	local height = 0

	-- title
	local row = ftable:addRow(false, { bgColor = Helper.color.transparent })
	local text = menu.texts.targetShortName
	if menu.construction then
		text = menu.texts.constructionName
	end
	text = TruncateText(text, Helper.standardFontBold, Helper.scaleFont(Helper.standardFontBold, Helper.headerRow1FontSize), menu.width - Helper.standardButtonWidth - 2 * config.entryX)
	row[1]:setColSpan(2):createText(text, Helper.headerRowCenteredProperties)
	row[1].properties.color = menu.colors.target
	height = height + row:getHeight() + Helper.borderSize

	-- entries
	local convertedComponent = ConvertStringTo64Bit(tostring(menu.componentSlot.component))
	local isonlinetarget, isplayerownedtarget
	if convertedComponent ~= 0 then
		isonlinetarget, isplayerownedtarget = GetComponentData(convertedComponent, "isonlineobject", "isplayerowned")
	end
	if (#menu.selectedplayerships == 0) and (#menu.selectednpcships > 0) then
		-- only npc ships are selected, show the player that they cannot do anything with an npc ship
		local row = ftable:addRow(false, { bgColor = Helper.defaultTitleBackgroundColor })
		row[1]:createText(ReadText(1001, 7804), { mouseOverText = menu.texts.npcFullNames })
		row[2]:createText(menu.texts.npcName, { halign = "right", color = menu.colors.selected, mouseOverText = menu.texts.npcFullNames })
		local row = ftable:addRow(true, { bgColor = Helper.color.transparent })
		local button = row[1]:setColSpan(2):createButton({ active = false, bgColor = Helper.color.darkgrey }):setText(ReadText(1001, 7852), { color = Helper.color.red })
	elseif isonlinetarget and isplayerownedtarget then
		local row = ftable:addRow(true, { fixed = true, bgColor = Helper.color.transparent })
		local button = row[1]:setColSpan(2):createButton({ active = false, bgColor = Helper.color.darkgrey }):setText(ReadText(1001, 7868), { color = Helper.color.red })
	else
		local first = true
		for _, section in ipairs(config.sections) do
			local pass = false
			if menu.showPlayerInteractions then
				if section.isplayerinteraction or (menu.shown and (not section.isorder)) then
					pass = true
				end
			elseif (section.isorder == nil) or (section.isorder == (#menu.selectedplayerships > 0)) then
				pass = true
			end

			if pass then
				if section.subsections then
					local hastitle = false
					for _, subsection in ipairs(section.subsections) do
						if (#menu.actions[subsection.id] > 0) or menu.forceSubSection[subsection.id] then
							if not hastitle then
								height = height + menu.addSectionTitle(ftable, section, first)
								first = false
								hastitle = true
							end
							local data = { id = subsection.id, y = height }
							local row = ftable:addRow(data, { bgColor = Helper.color.transparent })
							local iconHeight = Helper.scaleY(config.rowHeight)
							local button = row[1]:setColSpan(2):createButton({ active = #menu.actions[subsection.id] > 0, bgColor = Helper.color.transparent, mouseOverText = (#menu.actions[subsection.id] > 0) and "" or menu.forceSubSection[subsection.id] }):setText(subsection.text, { color = Helper.color.white }):setIcon("table_arrow_inv_right", { scaling = false, width = iconHeight, height = iconHeight, x = menu.width - iconHeight })
							row[1].handlers.onClick = function () return menu.handleSubSectionOption(data) end
							height = height + row:getHeight() + Helper.borderSize
						end
					end
				elseif #menu.actions[section.id] > 0 then
					height = height + menu.addSectionTitle(ftable, section, first)
					first = false
					local availabletextwidth
					if (section.id == "selected_orders") or (section.id == "trade_orders") or (section.id == "selected_assignments") or (section.id == "player_interaction") or (section.id == "trade") then
						local maxtextwidth = 0
						for _, entry in ipairs(menu.actions[section.id]) do
							if not entry.hidetarget then
								maxtextwidth = math.max(maxtextwidth, C.GetTextWidth(entry.text .. " ", Helper.standardFont, Helper.scaleFont(Helper.standardFont, config.entryFontSize, true)))
							end
						end
						availabletextwidth = menu.width - maxtextwidth - 2 * Helper.scaleX(config.entryX) - Helper.borderSize
					end

					for _, entry in ipairs(menu.actions[section.id]) do
						if entry.active == nil then
							entry.active = true
						end
						local row = ftable:addRow(true, { bgColor = Helper.color.transparent })
						local button = row[1]:setColSpan(2):createButton({
							bgColor = entry.active and Helper.color.transparent or Helper.color.darkgrey,
							highlightColor = entry.active and Helper.defaultButtonHighlightColor or Helper.defaultUnselectableButtonHighlightColor,
							mouseOverText = entry.mouseOverText
						}):setText(entry.text, { color = entry.active and Helper.color.white or Helper.color.lightgrey })
						button.properties.uiTriggerID = entry.type
						if (section.id == "selected_orders") or (section.id == "trade_orders") or (section.id == "selected_assignments") or (section.id == "player_interaction") or (section.id == "trade") then
							if not entry.hidetarget then
								local text2 = ""
								if entry.text2 then
									text2 = entry.text2
								else
									if ((section.id == "trade_orders") or (section.id == "trade")) and entry.buildstorage then
										text2 = menu.texts.buildstorageName
									else
										text2 = menu.texts.targetBaseName or menu.texts.targetShortName
									end
								end
								text2 = TruncateText(text2, button.properties.text.font, Helper.scaleFont(button.properties.text.font, button.properties.text.fontsize, button.properties.scaling), availabletextwidth)
								button:setText2(text2, { halign = "right", color = menu.colors.target })
								if entry.mouseOverText == nil then
									button.properties.mouseOverText = entry.text .. " " .. menu.texts.targetName
								end
							end
						end
						if entry.active then
							row[1].handlers.onClick = entry.script
						end
						height = height + row:getHeight() + Helper.borderSize
					end
				end
			end
		end
		if first then
			local row = ftable:addRow(true, { bgColor = Helper.color.transparent })
			local button = row[1]:setColSpan(2):createButton({ active = false, bgColor = Helper.color.darkgrey }):setText("---", { halign = "center", color = Helper.color.red })
		end
	end

	ftable:setSelectedRow(menu.selectedRows.contentTable)
	menu.selectedRows.contentTable = nil

	return ftable
end

function menu.createSubSectionTable(frame, position)
	local x = 0
	if position == "right" then
		x = menu.width + Helper.borderSize
	end

	local ftable = frame:addTable(2, { tabOrder = 1, x = x, width = menu.width, backgroundID = "solid", backgroundColor = Helper.color.semitransparent, highlightMode = "off" })
	ftable:setDefaultCellProperties("text",   { minRowHeight = config.rowHeight, fontsize = config.entryFontSize, x = config.entryX })
	ftable:setDefaultCellProperties("button", { height = config.rowHeight })
	ftable:setDefaultComplexCellProperties("button", "text", { fontsize = config.entryFontSize, x = config.entryX })
	ftable:setDefaultComplexCellProperties("button", "text2", { fontsize = config.entryFontSize, x = config.entryX })
	ftable:setColWidthPercent(2, 40)
	ftable:setDefaultBackgroundColSpan(1, 2)

	for _, entry in ipairs(menu.actions[menu.subsection.id]) do
		if entry.active == nil then
			entry.active = true
		end
		row = ftable:addRow(true, { bgColor = Helper.color.transparent })
		local maxtextwidth = 0
		if entry.text2 then
			maxtextwidth = C.GetTextWidth(entry.text2 .. " ", entry.text2font or Helper.standardFont, Helper.scaleFont(Helper.standardFont, config.entryFontSize, true))
		end
		local availabletextwidth = menu.width - maxtextwidth - 2 * Helper.scaleX(config.entryX) - Helper.borderSize

		local button = row[1]:setColSpan(2):createButton({ active = entry.active, bgColor = entry.active and Helper.color.transparent or Helper.color.darkgrey })
		local text = TruncateText(entry.text, button.properties.text.font, Helper.scaleFont(button.properties.text.font, button.properties.text.fontsize, button.properties.scaling), availabletextwidth)
		if entry.mouseOverText then
			button.properties.mouseOverText = entry.mouseOverText
		elseif text ~= entry.text then
			button.properties.mouseOverText = entry.text
		end
		button:setText(text, { color = entry.active and Helper.color.white or Helper.color.lightgrey })
		row[1].handlers.onClick = entry.script
		if entry.text2 then
			button:setText2(entry.text2, { halign = "right", color = entry.active and Helper.color.white or Helper.color.lightgrey, font = entry.text2font or Helper.standardFont }) 
		end
	end

	return ftable
end

-- helpers

function menu.getPlayerSquad()
	local playerSquad = {}

	local playerOccupiedShip = C.GetPlayerOccupiedShipID()
	if (playerOccupiedShip ~= 0) then
		local locplayersubordinates = GetSubordinates(ConvertStringTo64Bit(tostring(playerOccupiedShip)), nil, true)
		for _, subordinate in ipairs(locplayersubordinates) do
			playerSquad[ConvertIDTo64Bit(subordinate)] = true
			--print("inserting: " .. ConvertIDTo64Bit(subordinate))
		end
	end

	return playerSquad
end

function menu.setOrderImmediate(component, orderidx)
	local newidx = 1
	if not C.AdjustOrder(component, orderidx, newidx, true, false, true) then
		newidx = 2
	end
	C.AdjustOrder(component, orderidx, newidx, true, false, false)
end

function menu.handleSubSectionOption(data)
	if type(data) == "table" then
		if (not menu.subsection) or (menu.subsection.id ~= data.id) then
			if #menu.actions[data.id] > 0 then
				menu.subsection = data
			else
				menu.subsection = nil
			end
			menu.refresh = true
		end
	else
		if type(menu.subsection) == "table" then
			menu.subsection = nil
			menu.refresh = true
		end
	end
end

function menu.processSelectedPlayerShips()
	local convertedComponent = ConvertStringTo64Bit(tostring(menu.componentSlot.component))
	local isplayerownedtarget = false
	if convertedComponent ~= 0 then
		isplayerownedtarget = GetComponentData(convertedComponent, "isplayerowned")
	end
	local playercontainer = C.GetPlayerContainerID()
	local convertedPlayerContainer
	if playercontainer ~= 0 then
		convertedPlayerContainer = ConvertStringTo64Bit(tostring(playercontainer))
	end

	menu.possibleorders = {
		["Attack"] = false,
		["Board"] = false,
		["Collect"] = false,
		["CollectDropsInRadius"] = false,
		["DeployObjectAtPosition"] = false,
		["DockAndWait"] = false,
		["Explore"] = false,
		["ExploreUpdate"] = false,
		["Flee"] = false,
		["Follow"] = false,
		["MiningPlayer"] = false,
		["MoveWait"] = false,
		["Player_DockToTrade"] = false,
		["ProtectStation"] = false,
		["Repair"] = false,
	}
	menu.numdockingpossible = 0
	menu.numassignableships = 0
	menu.numassignableminingships = 0
	menu.numremovableorders = 0
	menu.numwaitingforsignal = 0
	menu.numdockingatplayerpossible = 0

	for i = #menu.selectedplayerships, 1, -1 do
		local ship = menu.selectedplayerships[i]
		if ship == menu.componentSlot.component then
			table.remove(menu.selectedplayerships, i)
		else
			-- Check orders
			for orderid, value in pairs(menu.possibleorders) do
				if not value then
					if C.IsOrderSelectableFor(orderid, ship) then
						menu.possibleorders[orderid] = true
					end
				end
			end
			
			-- Check assignments
			if isplayerownedtarget and C.IsComponentClass(menu.componentSlot.component, "controllable") then
				local commander = ConvertIDTo64Bit(GetCommander(ship))
				if commander ~= convertedComponent and C.CanAcceptSubordinate(menu.componentSlot.component, ship) then
					menu.numassignableships = menu.numassignableships + 1
					if GetComponentData(ship, "primarypurpose") == "mine" then
						menu.numassignableminingships = menu.numassignableminingships + 1
					end
				end
			end

			-- Check docking
			if C.IsComponentClass(menu.componentSlot.component, "container") then
				if (convertedComponent ~= 0) and IsDockingPossible(ship, convertedComponent) then
					menu.numdockingpossible = menu.numdockingpossible + 1
				end
			end
			if (playercontainer ~= 0) and IsDockingPossible(ship, convertedPlayerContainer) then
				menu.numdockingatplayerpossible = menu.numdockingatplayerpossible + 1
			end

			-- check order removal
			local numorders = C.GetNumOrders(ship)
			local currentorders = ffi.new("Order[?]", numorders)
			numorders = C.GetOrders(currentorders, numorders, ship)
			for i = numorders, 1, -1 do
				local isdocked, isdocking = GetComponentData(ship, "isdocked", "isdocking")
				if (i == 1) and ((ffi.string(currentorders[0].orderdef) == "DockAndWait") and (isdocked or isdocking)) then
					-- do nothing - removing the dock order would create an undock order ... rather have the ship stay put [Nick]
				else
					if C.RemoveOrder(ship, i, false, true) then
						menu.numremovableorders = menu.numremovableorders + 1
						break
					end
				end
			end

			-- check for waiting for signal
			local numorders = C.GetNumOrders(ship)
			if numorders > 0 then
				local orderparams = GetOrderParams(ship, 1)
				local iswaitingforsignal = false
				for i, param in ipairs(orderparams) do
					if param.name == "releasesignal" and type(param.value) == "table" and param.value[1] == "playerownedship_proceed" then
						menu.numwaitingforsignal = menu.numwaitingforsignal + 1
						break
					end
				end
			end
		end
	end
	local occupiedPlayerShip = ConvertStringTo64Bit(tostring(C.GetPlayerOccupiedShipID()))
	if (#menu.selectedplayerships == 1) and (menu.selectedplayerships[1] == occupiedPlayerShip) then
		menu.showPlayerInteractions = true
	else
		for i, ship in ipairs(menu.selectedplayerships) do
			if ship == occupiedPlayerShip then
				menu.removedOccupiedPlayerShip = occupiedPlayerShip
				table.remove(menu.selectedplayerships, i)
				break
			end
		end
	end

	if isplayerownedtarget and C.IsComponentClass(menu.componentSlot.component, "ship") then
		if (playercontainer ~= 0) and (convertedComponent ~= 0) and IsDockingPossible(convertedComponent, convertedPlayerContainer) then
			menu.numdockingatplayerpossible = menu.numdockingatplayerpossible + 1
		end
	end

	for i = #menu.selectednpcships, 1, -1 do
		local ship = menu.selectednpcships[i]
		if ship == menu.componentSlot.component then
			table.remove(menu.selectednpcships, i)
			break
		end
	end
end

function menu.prepareSections()
	menu.actions = {}
	for _, section in ipairs(config.sections) do
		if section.subsections then
			for _, subsection in ipairs(section.subsections) do
				menu.actions[subsection.id] = {}
			end
		else
			menu.actions[section.id] = {}
		end
	end
end

function menu.insertInteractionContent(section, entry)
	if menu.actions[section] then
		table.insert(menu.actions[section], entry)
	else
		DebugError("The requested context section is not defined: '" .. section .. "' [Florian]")
	end
end

config.consumables = {
	{ id = "satellite",		type = "civilian",	getnum = C.GetNumAllSatellites,		getdata = C.GetAllSatellites },
	{ id = "navbeacon",		type = "civilian",	getnum = C.GetNumAllNavBeacons,		getdata = C.GetAllNavBeacons },
	{ id = "resourceprobe",	type = "civilian",	getnum = C.GetNumAllResourceProbes,	getdata = C.GetAllResourceProbes },
	{ id = "lasertower",	type = "military",	getnum = C.GetNumAllLaserTowers,	getdata = C.GetAllLaserTowers },
	{ id = "mine",			type = "military",	getnum = C.GetNumAllMines,			getdata = C.GetAllMines },
}

function menu.addConsumableEntry(basesection, consumabledata, object, callback)
	local numconsumable = consumabledata.getnum(object)
	if numconsumable > 0 then
		local consumables = ffi.new("AmmoData[?]", numconsumable)
		numconsumable = consumabledata.getdata(consumables, numconsumable, object)
		for j = 0, numconsumable - 1 do
			if consumables[j].amount > 0 then
				local macro = ffi.string(consumables[j].macro)
				menu.insertInteractionContent(basesection .. "_" .. consumabledata.type, { type = consumabledata.type, text = GetMacroData(macro, "name"), text2 = "(" .. consumables[j].amount .. ")", script = function () return callback(consumabledata.id, macro, 1) end })
			end
		end
	end
end

function menu.insertLuaAction(actiontype, istobedisplayed)
	local convertedComponent = ConvertStringTo64Bit(tostring(menu.componentSlot.component))
	local isplayerownedtarget = GetComponentData(convertedComponent, "isplayerowned")
	local istargetinplayersquad = false
	local istargetplayeroccupiedship = false
	if (convertedComponent ~= 0) then
		istargetinplayersquad = menu.playerSquad[convertedComponent]
		--print("istargetinplayersquad: " .. tostring(istargetinplayersquad) .. ", commander: " .. tostring(GetCommander(convertedComponent)) .. ", occupiedship: " .. ConvertStringTo64Bit(tostring(C.GetPlayerOccupiedShipID())))
		istargetplayeroccupiedship = (ConvertStringTo64Bit(tostring(C.GetPlayerOccupiedShipID())) == convertedComponent)
		--print("istargetplayeroccupiedship: " .. tostring(istargetplayeroccupiedship) .. ", occupiedship: " .. ConvertStringTo64Bit(tostring(C.GetPlayerOccupiedShipID())) .. ", convertedComponent: " .. tostring(convertedComponent))
	end
	
	if actiontype == "accountmanagement" then
		if istobedisplayed then
			menu.insertInteractionContent("main", { type = actiontype, text = ReadText(1001, 7860), script = menu.buttonAccountManagement })
		end
	elseif actiontype == "assign" then
		if menu.numassignableships > 0 then
			menu.insertInteractionContent("selected_assignments", { type = actiontype, text =  "[" .. ReadText(1001, 7814) .. "] " .. ReadText(20208, 40303), text2 = Helper.convertColorToText(menu.holomapcolor.playercolor) .. ((menu.numassignableships == 1) and ReadText(1001, 7851) or string.format(ReadText(1001, 7801), menu.numassignableships)), script = function () menu.buttonAssignCommander("defence") end })
			if C.IsComponentClass(menu.componentSlot.component, "station") then
				if menu.numassignableminingships > 0 then
					menu.insertInteractionContent("selected_assignments", { type = actiontype, text = ReadText(20208, 40203), text2 = Helper.convertColorToText(menu.holomapcolor.playercolor) .. ((menu.numassignableminingships == 1) and ReadText(1001, 7851) or string.format(ReadText(1001, 7801), menu.numassignableminingships)), script = function () menu.buttonAssignCommander("mining") end })
				end
				local numnonminingships = menu.numassignableships - menu.numassignableminingships
				if numnonminingships > 0 then
					menu.insertInteractionContent("selected_assignments", { type = actiontype, text = ReadText(20208, 40103), text2 = Helper.convertColorToText(menu.holomapcolor.playercolor) .. ((numnonminingships == 1) and ReadText(1001, 7851) or string.format(ReadText(1001, 7801), numnonminingships)), script = function () menu.buttonAssignCommander("trade") end })
				end
			end
		end
	elseif actiontype == "attack" then
		if (#menu.selectedplayerships > 0) and menu.possibleorders["Attack"] and (not isplayerownedtarget) and C.IsComponentClass(menu.componentSlot.component, "destructible") then
			menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7815), script = function () return menu.buttonAttack(false) end })
		end
	elseif actiontype == "attackmultiple" then
		if (#menu.selectedplayerships > 0) and menu.possibleorders["Attack"] and (not isplayerownedtarget) and C.IsComponentClass(menu.componentSlot.component, "destructible") then
			menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7816), script = function () return menu.buttonAttackMultiple(false) end, hidetarget = true })
		end
	elseif actiontype == "attackplayertarget" then
		if (istargetinplayersquad or istargetplayeroccupiedship) and GetPlayerTarget() then
			--print("player target: " .. tostring(GetPlayerTarget()))
			menu.insertInteractionContent("playersquad_orders", { type = actiontype, text = ReadText(1001, 7869), script = function () return menu.buttonPlayerSquadAttackPlayerTarget(false) end, hidetarget = true })	-- Wing: Attack my target
		end
	elseif actiontype == "board" then
		if (#menu.selectedplayerships > 0) and menu.possibleorders["Board"] and (not isplayerownedtarget) and (C.IsComponentClass(menu.componentSlot.component, "ship_l") or C.IsComponentClass(menu.componentSlot.component, "ship_xl")) then
			menu.insertInteractionContent(menu.showPlayerInteractions and "player_interaction" or "selected_orders", { type = actiontype, text = ReadText(1001, 7842), script = function() return menu.buttonBoard() end })
		end
	elseif actiontype == "build" then
		if (#menu.selectedplayerships == 1) and isplayerownedtarget and (GetComponentData(menu.selectedplayerships[1], "primarypurpose") == "build") then
			local buildstorage
			if C.IsComponentClass(menu.componentSlot.component, "station") then
				buildstorage = ConvertIDTo64Bit(GetComponentData(convertedComponent, "buildstorage")) or 0
			else
				buildstorage = convertedComponent
			end
			local constructionvessels = {}
			Helper.ffiVLA(constructionvessels, "UniverseID", C.GetNumAssignedConstructionVessels, C.GetAssignedConstructionVessels, buildstorage)
			local mouseovertext
			if C.IsBuilderBusy(menu.selectedplayerships[1]) then
				mouseovertext = ReadText(1026, 7820)
			elseif #constructionvessels > 0 then
				mouseovertext = ReadText(1026, 7821)
			end
			menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7833), script = function () return menu.buttonDeployToStation(menu.selectedplayerships[1], false) end, active = (#constructionvessels == 0) and (not C.IsBuilderBusy(menu.selectedplayerships[1])), mouseOverText = mouseovertext })
		end
	elseif actiontype == "buildships" then
		local canbuildships, shiptrader, isdock = GetComponentData(convertedComponent, "canbuildships", "shiptrader", "isdock")
		if canbuildships and shiptrader and isdock then
			menu.insertInteractionContent("main", { type = actiontype, text = ReadText(1001, 7838), script = function () return menu.buttonShipConfig("purchase") end })
		end
	elseif actiontype == "changeformation" then
		if istobedisplayed then
			local subordinates = GetSubordinates(convertedComponent)
			if #subordinates > 0 then
				local n = C.GetNumFormationShapes()
				local buf = ffi.new("UIFormationInfo[?]", n)
				n = C.GetFormationShapes(buf, n)
				local formationshapes = {}
				for i = 0, n - 1 do
					table.insert(formationshapes, { name = ffi.string(buf[i].name), shape = ffi.string(buf[i].shape), requiredSkill = buf[i].requiredSkill })
				end
				table.sort(formationshapes, Helper.sortName)
				local aipilot = GetComponentData(convertedComponent, "assignedaipilot")
				local adjustedskill = aipilot and math.floor(C.GetEntityCombinedSkill(ConvertIDTo64Bit(aipilot), nil, "aipilot") * 5 / 100) or 0
				for _, data in ipairs(formationshapes) do
					menu.insertInteractionContent("formationshape", { type = actiontype, text = data.name, text2 = "\27Y" .. string.rep(utf8.char(9733), data.requiredSkill) .. string.rep(utf8.char(9734), 5 - data.requiredSkill), text2font = Helper.starFont, script = function () return menu.buttonFormationShape(data.shape, subordinates) end, active = data.requiredSkill <= adjustedskill })
				end
			end
		end
	elseif actiontype == "configurestation" then
		if istobedisplayed then
			menu.insertInteractionContent("main", { type = actiontype, text = ReadText(1001, 7809), script = menu.buttonStationConfig })
		end
	elseif actiontype == "collect" then
		if #menu.selectedplayerships > 0 and menu.possibleorders["Collect"] and menu.componentSlot.component and C.IsComponentClass(menu.componentSlot.component, "drop") then
			menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7867), script = function () return menu.buttonCollect(false) end, hidetarget = true } )
		end
	elseif actiontype == "collectspace" then
		if menu.offsetcomponent and (menu.offsetcomponent ~= 0) then
			if #menu.selectedplayerships > 0 and menu.possibleorders["CollectDropsInRadius"] then
				menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7866), script = function () return menu.buttonCollectRadius(false) end, hidetarget = true } )
			end
		end
	elseif actiontype == "deployhere" then
		if istobedisplayed then
			-- force sub section and assume we have no deployables
			menu.forceSubSection["consumables_civilian"] = ReadText(1026, 7818)
			menu.forceSubSection["consumables_military"] = ReadText(1026, 7819)
			-- check if we have deploables
			for _, entry in ipairs(config.consumables) do
				local numconsumable = entry.getnum(menu.componentSlot.component)
				if numconsumable > 0 then
					local consumables = ffi.new("AmmoData[?]", numconsumable)
					numconsumable = entry.getdata(consumables, numconsumable, menu.componentSlot.component)
					for j = 0, numconsumable - 1 do
						if consumables[j].amount > 0 then
							-- clear force if we do have deployables, enabling the isdocked check to set it's own force reason
							menu.forceSubSection["consumables_" .. entry.type] = nil
							break
						end
					end
				end
			end

			local isdocked = GetComponentData(convertedComponent, "isdocked")
			if not isdocked then
				for _, entry in ipairs(config.consumables) do
					menu.addConsumableEntry("consumables", entry, menu.componentSlot.component, menu.buttonDeploy)
				end
			else
				-- only force if not previously forced (i.e. no deployables is a more important reason than being docked)
				if not menu.forceSubSection["consumables_civilian"] then
					menu.forceSubSection["consumables_civilian"] = ReadText(1026, 7817)
				end
				if not menu.forceSubSection["consumables_military"] then
					menu.forceSubSection["consumables_military"] = ReadText(1026, 7817)
				end
			end
		end
	elseif actiontype == "deployat" then
		if menu.offsetcomponent and (menu.offsetcomponent ~= 0) then
			if (#menu.selectedplayerships == 1) and menu.possibleorders["DeployObjectAtPosition"] then
				menu.forceSubSection["selected_consumables_civilian"] = ReadText(1026, 7818)
				menu.forceSubSection["selected_consumables_military"] = ReadText(1026, 7819)
				for _, entry in ipairs(config.consumables) do
					menu.addConsumableEntry("selected_consumables", entry, menu.selectedplayerships[1], menu.buttonDeployAtPosition)
				end
			end
		end
	elseif actiontype == "dockat" then
		if menu.possibleorders["DockAndWait"] and GetComponentData(convertedComponent, "isdock") and (menu.numdockingpossible > 0) then
			menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7818), script = function () return menu.buttonDock(false) end })
		end
	elseif actiontype == "dockatplayer" then
		if istobedisplayed then
			if menu.mode ~= "shipconsole" then
				local playercontainer = C.GetPlayerContainerID()
				if playercontainer ~= 0 then
					local convertedPlayerContainer = ConvertStringTo64Bit(tostring(playercontainer))
					if (menu.possibleorders["DockAndWait"] or IsDockingPossible(convertedComponent, convertedPlayerContainer)) and GetComponentData(convertedPlayerContainer, "isdock") and (menu.numdockingatplayerpossible > 0) then
						menu.insertInteractionContent(((not menu.showPlayerInteractions) and (#menu.selectedplayerships > 0)) and "selected_orders_all" or "main_orders", { type = actiontype, text = ReadText(1001, 7837), script = function () return menu.buttonDockAtPlayer(false) end, hidetarget = true })
					end
				end
			end
		end
	elseif actiontype == "dockrequest" then
		if menu.showPlayerInteractions and IsDockingPossible(ConvertStringTo64Bit(tostring(C.GetPlayerOccupiedShipID())), convertedComponent) then
			menu.insertInteractionContent("player_interaction", { type = actiontype, text = ReadText(1001, 7845), script = menu.buttonDockRequest, active = C.RequestDockAt(menu.componentSlot.component, true) })
		end
	elseif actiontype == "explore" then
		if #menu.selectedplayerships > 0 and menu.possibleorders["Explore"] and menu.componentSlot.component and (C.IsComponentClass(menu.componentSlot.component, "sector") or C.IsComponentClass(menu.componentSlot.component, "highwayentrygate") or (C.IsComponentClass(menu.componentSlot.component, "gate") and GetComponentData(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), "isactive"))) then
			menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7828), script = function () return menu.buttonExplore(false) end })
		end
	elseif actiontype == "exploreupdate" then
		if #menu.selectedplayerships > 0 and menu.possibleorders["ExploreUpdate"] and menu.componentSlot.component and (C.IsComponentClass(menu.componentSlot.component, "sector") or C.IsComponentClass(menu.componentSlot.component, "highwayentrygate") or (C.IsComponentClass(menu.componentSlot.component, "gate") and GetComponentData(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), "isactive"))) then
			menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7829), script = function () return menu.buttonExploreUpdate(false) end })
		end
	elseif actiontype == "flyto" then
		if #menu.selectedplayerships > 0 and menu.possibleorders["MoveWait"] then
			menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7806), script = function () return menu.buttonMoveWait(false) end })
		end
	elseif actiontype == "follow" then
		if #menu.selectedplayerships > 0 and menu.possibleorders["Follow"] then
			menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7835), script = function () return menu.buttonFollow(false) end })
		end
	elseif actiontype == "guidance" then
		if menu.mode ~= "shipconsole" then
			if C.IsComponentClass(menu.componentSlot.component, "sector") then
				menu.insertInteractionContent(menu.showPlayerInteractions and "player_interaction" or "interaction", { type = actiontype, text = IsSameComponent(GetActiveGuidanceMissionComponent(), convertedComponent) and ReadText(1001, 3243) or ReadText(1001, 3242), script = function () return menu.buttonGuidance(true) end })
			else
				menu.insertInteractionContent(menu.showPlayerInteractions and "player_interaction" or "interaction", { type = actiontype, text = IsSameComponent(GetActiveGuidanceMissionComponent(), convertedComponent) and ReadText(1001, 3243) or ReadText(1001, 3256), script = function () return menu.buttonGuidance(false) end })
			end
		end
	elseif actiontype == "hire" then
		if (not isplayerownedtarget) and (GetComponentData(convertedComponent, "primarypurpose") == "build") then
			local stations = GetContainedStationsByOwner("player", nil, true)
			for i = #stations, 1, -1 do
				local buildstorage, requiresbuilder = GetComponentData(stations[i], "buildstorage", "requiresbuilder")
				if not requiresbuilder then
					table.remove(stations, i)
				elseif C.GetNumAssignedConstructionVessels(ConvertIDTo64Bit(buildstorage)) > 0 then
					table.remove(stations, i)
				end
			end
			if #stations > 0 then
				local playermoney = GetPlayerMoney()
				local fee = tonumber(C.GetBuilderHiringFee())
				if playermoney < fee then
					menu.forceSubSection["hiringbuilder"] = ReadText(1026, 2966)
				elseif C.IsBuilderBusy(menu.componentSlot.component) then
					menu.forceSubSection["hiringbuilder"] = ReadText(1026, 7820)
				else
					local mouseover = ((fee > playermoney) and "\027R" or "\027G") .. ReadText(1001, 7940) .. ReadText(1001, 120) .. " " .. ConvertMoneyString(fee, false, true, nil, true) .. " " .. ReadText(1001, 101) .. "\027X"
					for i, station in ipairs(stations) do
						local station64 = ConvertIDTo64Bit(station)
						menu.insertInteractionContent("hiringbuilder", { type = actiontype, text = Helper.convertColorToText(menu.holomapcolor.playercolor) .. ffi.string(C.GetComponentName(station64)) .. " (" .. ffi.string(C.GetObjectIDCode(station64)) .. ")", script = function () return menu.buttonDeployToStation(menu.componentSlot.component, false, station64) end, mouseOverText = mouseover })
					end
				end
			end
		end
	elseif actiontype == "logicalstationoverview" then
		menu.insertInteractionContent("main", { type = actiontype, text = ReadText(1001,8401), script = menu.buttonStationOverview })
	elseif actiontype == "manageassignments" then
		if istobedisplayed then
			local commander = ConvertIDTo64Bit(GetCommander(convertedComponent))
			if commander and commander ~= 0 then
				menu.insertInteractionContent("main_assignments", { type = actiontype, text = ReadText(1001, 7810), script = function () menu.buttonRemoveAssignment() end })
				local currentassignment, primarypurpose = GetComponentData(convertedComponent, "assignment", "primarypurpose")
				if currentassignment ~= "defence" then
					menu.insertInteractionContent("main_assignments", { type = actiontype, text = ReadText(1001, 7811), script = function () menu.buttonChangeAssignment("defence") end })
				end
				if C.IsComponentClass(commander, "station") then
					if currentassignment ~= "mining" then
						if primarypurpose == "mine" then
							menu.insertInteractionContent("main_assignments", { type = actiontype, text = ReadText(1001, 7812), script = function () menu.buttonChangeAssignment("mining") end })
						end
					end
					if currentassignment ~= "trade" then
						if primarypurpose ~= "mine" then
							menu.insertInteractionContent("main_assignments", { type = actiontype, text = ReadText(1001, 7813), script = function () menu.buttonChangeAssignment("trade") end })
						end
					end
				end
			end
		end
	elseif actiontype == "mining" then
		if #menu.selectedplayerships > 0 and menu.possibleorders["MiningPlayer"] then
			if menu.offsetcomponent and (menu.offsetcomponent ~= 0) and C.IsComponentClass(menu.offsetcomponent, "sector") then
				local pos = ffi.new("Coord3D", { x = menu.offset.x, y = menu.offset.y, z = menu.offset.z })
				local nummineables = C.GetNumMineablesAtSectorPos(menu.offsetcomponent, pos)
				local mineables = ffi.new("YieldInfo[?]", nummineables)
				nummineables = C.GetMineablesAtSectorPos(mineables, nummineables, menu.offsetcomponent, pos)
				local miningwares = {}
				for i = 0, nummineables - 1 do
					if mineables[i].amount > 10 then
						local ware = ffi.string(mineables[i].wareid)
						table.insert(miningwares, { ware = ware, name = GetWareData(ware, "name"), amount = 0 })
						local entry = miningwares[#miningwares]
						for _, ship in ipairs(menu.selectedplayerships) do
							if GetWareCapacity(ship, ware, true) > 0 then
								entry.amount = entry.amount + 1
							end
						end
					end
				end
				table.sort(miningwares, Helper.sortName)
				local found = false
				for _, entry in ipairs(miningwares) do
					if entry.amount > 0 then
						found = true
						menu.insertInteractionContent("mining", { type = actiontype, text = entry.name, text2 = Helper.convertColorToText(menu.holomapcolor.playercolor) .. ((entry.amount == 1) and ReadText(1001, 7851) or string.format(ReadText(1001, 7801), entry.amount)), script = function () return menu.buttonMining(entry.ware, false) end })
					end
				end
				if not found then
					menu.forceSubSection["mining"] = ReadText(1026, 7823)
				end
			end
		end
	elseif actiontype == "orderoverview" then
		if istobedisplayed then
			menu.insertInteractionContent("main_orders", { type = actiontype, text = ReadText(1001, 7839), script = menu.buttonStandingOrders })
		end
	elseif actiontype == "paintmod" then
		if istobedisplayed then
			menu.insertInteractionContent(((not menu.showPlayerInteractions) and (#menu.selectedplayerships > 0)) and "selected" or "interaction", { type = actiontype, text = ReadText(1001, 7859), script = menu.buttonPaintMod })
		end
	elseif actiontype == "player_docktotrade" then
		local tradeoffers = GetTradeList(convertedComponent)
		if menu.possibleorders["Player_DockToTrade"] and (#tradeoffers == 0) and GetComponentData(convertedComponent, "isdock") and (menu.numdockingpossible > 0) then
			menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7858), script = function () return menu.buttonPlayerDockToTrade(false) end })	-- Dock to trade
		end
	elseif actiontype == "proceedwithorders" then
		if istobedisplayed and isplayerownedtarget then
			local iswaitingforsignal = false
			local numorders = C.GetNumOrders(menu.componentSlot.component)
			if numorders > 0 then
				local orderparams = GetOrderParams(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), 1)
				for i, param in ipairs(orderparams) do
					if param.name == "releasesignal" and type(param.value) == "table" and param.value[1] == "playerownedship_proceed" then
						iswaitingforsignal = true
						break
					end
				end
			end

			local infotext = ""
			if iswaitingforsignal then
				if menu.numwaitingforsignal == 0 then
					-- only the currently targeted ship is affected, show nothing
					infotext = ""
				else
					-- target ship and selected ships
					infotext = " \27G(" .. menu.texts.targetShortName .. " + " .. ((menu.numwaitingforsignal == 1) and ReadText(1001, 7851) or string.format(ReadText(1001, 7801), menu.numwaitingforsignal)) .. ")"
				end
			elseif menu.numwaitingforsignal > 0 then
				infotext = " \27G(" .. ((menu.numwaitingforsignal == 1) and ReadText(1001, 7851) or string.format(ReadText(1001, 7801), menu.numwaitingforsignal)) .. ")"
			end
			if iswaitingforsignal or (menu.numwaitingforsignal > 0) then
				menu.insertInteractionContent(((not menu.showPlayerInteractions) and (#menu.selectedplayerships > 0) and (menu.numremovableorders > 0)) and "selected_orders" or "main_orders", { type = actiontype, text = ReadText(1002, 2033) .. infotext, script = menu.buttonProceedWithOrders, hidetarget = true })
			end
		end
	elseif actiontype == "protectstation" then
		if #menu.selectedplayerships > 0 and isplayerownedtarget and menu.possibleorders["ProtectStation"] then
			menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7834), script = function () return menu.buttonProtect(false) end })
		end
	elseif actiontype == "recallsubs" then
		if istobedisplayed and C.IsComponentClass(menu.componentSlot.component, "controllable") then
			local subordinates = GetSubordinates(convertedComponent)
			if #subordinates > 0 then
				menu.insertInteractionContent("main_orders", { type = actiontype, text = ReadText(1001, 7830), script = function () return menu.buttonRecallSubordinates(menu.componentSlot.component, subordinates) end })
			end
		end
	elseif actiontype == "removeallorders" then
		if istobedisplayed then
			local hasremoveableorders = false
			local numorders = C.GetNumOrders(menu.componentSlot.component)
			local currentorders = ffi.new("Order[?]", numorders)
			numorders = C.GetOrders(currentorders, numorders, menu.componentSlot.component)
			for i = numorders, 1, -1 do
				local isdocked, isdocking = GetComponentData(convertedComponent, "isdocked", "isdocking")
				if (i == 1) and ((ffi.string(currentorders[0].orderdef) == "DockAndWait") and (isdocked or isdocking)) then
					-- do nothing - removing the dock order would create an undock order ... rather have the ship stay put [Nick]
				else
					if C.RemoveOrder(menu.componentSlot.component, i, false, true) then
						hasremoveableorders = true
						break
					end
				end
			end
			local infotext = ""
			if hasremoveableorders then
				if menu.numremovableorders == 0 then
					-- only the currently targeted ship is affected, show nothing
					infotext = ""
				else
					-- target ship and selected ships
					infotext = " \27G(" .. menu.texts.targetShortName .. " + " .. ((menu.numremovableorders == 1) and ReadText(1001, 7851) or string.format(ReadText(1001, 7801), menu.numremovableorders)) .. ")"
				end
			elseif menu.numremovableorders > 0 then
				infotext = " \27G(" .. ((menu.numremovableorders == 1) and ReadText(1001, 7851) or string.format(ReadText(1001, 7801), menu.numremovableorders)) .. ")"
			end
			menu.insertInteractionContent(((not menu.showPlayerInteractions) and (#menu.selectedplayerships > 0) and (menu.numremovableorders > 0)) and "selected_orders" or "main_orders", { type = actiontype, text = ReadText(1001, 7832) .. infotext, script = menu.buttonRemoveAllOrders, active = hasremoveableorders or (menu.numremovableorders > 0), hidetarget = true })
		end
	elseif actiontype == "sellships" then
		if #menu.selectedplayerships > 0 then
			if not isplayerownedtarget then
				local shiptrader, isdock = GetComponentData(convertedComponent, "shiptrader", "isdock")
				if shiptrader and isdock then
					menu.insertInteractionContent("selected_orders", { type = actiontype, text = (#menu.selectedplayerships == 1) and ReadText(1001, 7855) or ReadText(1001, 7856), script = menu.buttonSellShips })
				end
			end
		end
	elseif actiontype == "stopandholdfire" then
		if istargetinplayersquad or istargetplayeroccupiedship then
			menu.insertInteractionContent("playersquad_orders", { type = actiontype, text = ReadText(1001, 7870), script = function () return menu.buttonPlayerSquadStopAndHoldFire(true) end, hidetarget = true })	-- Wing: Stop and hold fire
		end
	elseif actiontype == "targetview" then
		if menu.mode ~= "shipconsole" then
			local playersector = C.GetContextByClass(C.GetPlayerID(), "sector", false)
			local targetsector = C.GetContextByClass(menu.componentSlot.component, "sector", false)
			local active = true
			local mouseovertext
			if (not C.IsPlayerCameraTargetViewPossible(menu.componentSlot.component, true)) or (playersector ~= targetsector) then
				active = false
				mouseovertext = ReadText(1026, 7809)
			elseif menu.componentSlot.component == C.GetPlayerControlledShipID() then
				active = false
				mouseovertext = ReadText(1026, 7810)
			end
			menu.insertInteractionContent("interaction", { type = actiontype, text = ReadText(1001, 7807), script = menu.buttonExternal, active = active, mouseOverText = mouseovertext })
		end
	elseif actiontype == "teleport" then
		if isplayerownedtarget or (C.IsComponentClass(menu.componentSlot.component, "station") and GetComponentData(convertedComponent, "isally")) then
			if menu.componentSlot.component ~= ConvertStringTo64Bit(tostring(C.GetPlayerOccupiedShipID())) then
				local active = false
				local mouseovertext
				local teleportrequest = ffi.string(C.CanTeleportPlayerTo(menu.componentSlot.component, false, (menu.mode == "shipconsole") and isplayerownedtarget))
				if teleportrequest == "granted" then
					active = true
				elseif teleportrequest == "instorage" then
					mouseovertext = ReadText(1026, 7811)
				elseif teleportrequest == "malfunction" then
					mouseovertext = ReadText(1026, 7812)
				elseif teleportrequest == "research" then
					mouseovertext = ReadText(1026, 7813)
				elseif teleportrequest == "range" then
					mouseovertext = ReadText(1026, 7814)
				elseif teleportrequest == "size" then
					mouseovertext = ReadText(1026, 7815)
				elseif teleportrequest == "slot" then
					mouseovertext = ReadText(1026, 7816)
				end
				menu.insertInteractionContent("interaction", { type = actiontype, text = (menu.mode == "shipconsole") and ReadText(1001, 7854) or ReadText(1001, 7808), script = menu.buttonTeleport, active = active, mouseOverText = mouseovertext })
			end
		end
	elseif actiontype == "upgrade" then
		local shiptrader, isdock = GetComponentData(convertedComponent, "shiptrader", "isdock")
		if #menu.selectedplayerships > 0 and menu.possibleorders["Repair"] and shiptrader and isdock then
			local active = false
			for _, ship in ipairs(menu.selectedplayerships) do
				if C.CanContainerEquipShip(menu.componentSlot.component, ship) then
					active = true
					break
				end
			end
			local mouseovertext
			if (not active) and (#menu.selectedplayerships > 0) then
				-- if the option is inactive, all ships are either capships or not, so only check the first one
				if C.IsComponentClass(menu.selectedplayerships[1], "ship_l") or C.IsComponentClass(menu.selectedplayerships[1], "ship_xl") then
					mouseovertext = ReadText(1026, 7805)
				else
					mouseovertext = ReadText(1026, 7804)
				end
			end
			menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7826), script = function () return menu.buttonUpgrade(true) end, active = active, mouseOverText = mouseovertext })
		end
	elseif actiontype == "upgradeships" then
		local shiptrader, isdock = GetComponentData(convertedComponent, "shiptrader", "isdock")
		local dockedships = {}
		if C.IsComponentClass(menu.componentSlot.component, "container") then
			Helper.ffiVLA(dockedships, "UniverseID", C.GetNumDockedShips, C.GetDockedShips, menu.componentSlot.component, "player")
		end
		if shiptrader and isdock then
			local active = false
			for _, ship in ipairs(dockedships) do
				if C.CanContainerEquipShip(menu.componentSlot.component, ship) then
					active = true
					break
				end
			end
			local mouseovertext
			if not active then
				if #dockedships > 0 then
					-- if the option is inactive, all ships are either capships or not, so only check the first one
					if C.IsComponentClass(dockedships[1], "ship_l") or C.IsComponentClass(dockedships[1], "ship_xl") then
						mouseovertext = ReadText(1026, 7807)
					else
						mouseovertext = ReadText(1026, 7806)
					end
				else
					mouseovertext = ReadText(1026, 7808)
				end
			end
			menu.insertInteractionContent("main", { type = actiontype, text = ReadText(1001, 7841), script = function () return menu.buttonShipConfig("upgrade") end, active = active, mouseOverText = mouseovertext })
		end
	elseif actiontype == "venturedockat" then
		if menu.possibleorders["DockAndWait"] and GetComponentData(convertedComponent, "isdock") then
			local ventureplatforms = {}
			Helper.ffiVLA(ventureplatforms, "UniverseID", C.GetNumVenturePlatforms, C.GetVenturePlatforms, menu.componentSlot.component)
			for _, platform in ipairs(ventureplatforms) do
				local hasdock = false
				for i = #menu.selectedplayerships, 1, -1 do
					local ship = menu.selectedplayerships[i]
					if C.IsComponentClass(menu.componentSlot.component, "container") then
						if C.HasVenturerDock(menu.componentSlot.component, ship, platform) then
							hasdock = true
							break
						end
					end
				end
				if hasdock then
					local docks = {}
					Helper.ffiVLA(docks, "UniverseID", C.GetNumVenturePlatformDocks, C.GetVenturePlatformDocks, platform)
					local counts = {
						["XL_L"] = 0,
						["M_S"] = 0,
					}
					for _, dock in ipairs(docks) do
						local docksizes = GetComponentData(ConvertStringTo64Bit(tostring(dock)), "docksizes")
						-- docksizes always return the biggest possible size of a dockingbay contained in the dockarea
						counts["XL_L"] = counts["XL_L"] + docksizes.docks_xl + docksizes.docks_l
						counts["M_S"]  = counts["M_S"]  + docksizes.docks_m  + docksizes.docks_s
					end
					local dockstring = ""
					if counts["XL_L"] > 0 then
						if dockstring ~= "" then
							dockstring = dockstring .. " "
						end
						dockstring = dockstring .. "[" .. counts["XL_L"] .. ReadText(1001, 42) .. " " .. ReadText(1001, 7863) .. "]"
					end
					if counts["M_S"] > 0 then
						if dockstring ~= "" then
							dockstring = dockstring .. " "
						end
						dockstring = dockstring .. "[" .. counts["M_S"] .. ReadText(1001, 42) .. " " .. ReadText(1001, 7864) .. "]"
					end
					menu.insertInteractionContent("venturedock", { type = actiontype, text = ffi.string(C.GetComponentName(platform)), text2 = dockstring, script = function () return menu.buttonDock(false, platform) end })
				end
			end
		end
	elseif actiontype == "wareexchange" then
		if #menu.selectedplayerships > 0 and isplayerownedtarget then
			if C.IsComponentClass(menu.componentSlot.component, "container") then
				menu.insertInteractionContent("trade_orders", { type = actiontype, text = ReadText(1001, 7820), script = function () return menu.buttonTrade(true) end })
			end
			if menu.buildstorage then
				menu.insertInteractionContent("trade_orders", { type = actiontype, text = ReadText(1001, 7820), script = function () return menu.buttonTrade(true, menu.buildstorage) end, buildstorage = true })
			end
		end
	elseif actiontype == "withdrawandhold" then
		if istargetinplayersquad or istargetplayeroccupiedship then
			menu.insertInteractionContent("playersquad_orders", { type = actiontype, text = ReadText(1001, 7871), script = function () return menu.buttonPlayerSquadWithdrawAndHold(true) end, hidetarget = true })	-- Wing: Withdraw and hold
		end
	elseif actiontype == "withdrawfromcombat" then
		if istargetinplayersquad or istargetplayeroccupiedship then
			menu.insertInteractionContent("playersquad_orders", { type = actiontype, text = ReadText(1001, 7872), script = function () return menu.buttonPlayerSquadWithdrawFromCombat(true) end, hidetarget = true })	-- Wing: Withdraw from combat
		end
	elseif actiontype == "cheat_satellite" then
		if menu.offsetcomponent and (menu.offsetcomponent ~= 0) then
			menu.insertInteractionContent("cheats", { type = actiontype, text = "Place satellite", script = menu.buttonSatelliteCheat }) -- (cheat only)
		end
	elseif actiontype == "cheat_navbeacon" then
		if menu.offsetcomponent and (menu.offsetcomponent ~= 0) then
			menu.insertInteractionContent("cheats", { type = actiontype, text = "Place nav beacon", script = menu.buttonNavBeaconCheat }) -- (cheat only)
		end
	elseif actiontype == "cheat_resourceprobe" then
		if menu.offsetcomponent and (menu.offsetcomponent ~= 0) then
			menu.insertInteractionContent("cheats", { type = actiontype, text = "Place resource probe", script = menu.buttonResourceProbeCheat }) -- (cheat only)
		end
	elseif actiontype == "cheat_takeownership" then
		if not isplayerownedtarget then
			menu.insertInteractionContent("cheats", { type = actiontype, text = "Take ownership", script = menu.buttonOwnerCheat }) -- (cheat only)
		end
	elseif actiontype == "cheat_warp" then
		if menu.offsetcomponent and (menu.offsetcomponent ~= 0) then
			menu.insertInteractionContent("cheats", { type = actiontype, text = "Warp here", script = menu.buttonWarpCheat }) -- (cheat only)
		end
	else
		DebugError("Unknown LuaAction type '" .. actiontype .. "'! [Florian]")
	end
end

function menu.prepareActions()
	menu.checkPlayerActivity = nil
	menu.forceSubSection = {}
	menu.prepareSections()

	-- player actions
	if (not menu.componentOrder) and (not menu.construction) and (not menu.mission) and (not menu.missionoffer) then
		local isknown = C.IsObjectKnown(menu.componentSlot.component)
		local n = C.GetNumCompSlotPlayerActions(menu.componentSlot)
		local buf = ffi.new("UIAction[?]", n)
		n = C.GetCompSlotPlayerActions(buf, n, menu.componentSlot)
		for i = 0, n - 1 do
			local entry = {}
			entry.id = buf[i].id
			entry.text = ffi.string(buf[i].text)
			entry.active = buf[i].ispossible
			local actiontype = ffi.string(buf[i].type)
			if (not menu.shown) and (actiontype == "containertrade") then
				entry.script = function () return menu.buttonTrade(false) end
			elseif (not menu.shown) and (actiontype == "info") then
				entry.script = menu.buttonInfo
			elseif (not menu.shown) and (actiontype == "comm") then
				entry.script = menu.buttonComm
			else
				entry.script = function () return menu.buttonPerformPlayerAction(entry.id, actiontype) end
			end

			local basetype, luatype = string.match(actiontype, "(.+);(.+)")
			if isknown or (actiontype == "info") or (luatype == "guidance") then
				if basetype == "lua" then
					menu.insertLuaAction(luatype, buf[i].istobedisplayed)
				elseif buf[i].istobedisplayed then
					if actiontype == "containertrade" then
						if (not menu.showPlayerInteractions) and (#menu.selectedplayerships > 0) then
							if C.IsComponentClass(menu.componentSlot.component, "container") then
								menu.insertInteractionContent("trade_orders", entry)
							end
							if menu.buildstorage then
								menu.insertInteractionContent("trade_orders", { text = ReadText(1001, 7819), script = function () return menu.buttonTrade(false, menu.buildstorage) end, buildstorage = true })
							end
						else
							entry.text = ReadText(1001, 1113)
							if C.IsComponentClass(menu.componentSlot.component, "container") then
								menu.insertInteractionContent("trade", entry)
							end
							if menu.buildstorage then
								menu.insertInteractionContent("trade", { text = ReadText(1001, 1113), script = function () return menu.buttonTrade(false, menu.buildstorage) end, buildstorage = true })
							end
						end
					elseif actiontype == "hack" then
						if menu.showPlayerInteractions then
							menu.insertInteractionContent("player_interaction", entry)
						end
					elseif actiontype == "scan" then
						if menu.showPlayerInteractions then
							if not entry.active then
								if GetPlayerActivity() ~= "scan" then
									entry.mouseOverText = ReadText(1026, 7803)
								else
									entry.mouseOverText = ReadText(1026, 7809)
								end
							end
							menu.checkPlayerActivity = true
							menu.insertInteractionContent("player_interaction", entry)
						end
					elseif (actiontype == "comm") then
						if not entry.active then
							if GetControlEntity(ConvertStringTo64Bit(tostring(menu.componentSlot.component))) then
								entry.mouseOverText = ReadText(1026, 7802)
							else
								entry.mouseOverText = ReadText(1026, 7801)
							end
						end
						menu.insertInteractionContent("interaction", entry)
					elseif (actiontype == "claim") then
						menu.insertInteractionContent("interaction", entry)
					else
						menu.insertInteractionContent("main", entry)
					end
				end
			end
		end
		
		local convertedComponent = ConvertStringTo64Bit(tostring(menu.componentSlot.component))
		local canbuildships, canequipships, isplayerownedtarget = GetComponentData(convertedComponent, "canbuildships", "canequipships", "isplayerowned")
		
		-- DebugError("SHIPEPE1: ".. tostring(canbuildships) ..", ".. tostring(canequipships))
		
		if C.IsComponentClass(convertedComponent, "ship") and isplayerownedtarget then
			if canbuildships then --Ship on deck building
				menu.insertInteractionContent("main", { type = actiontype, text = ReadText(1001, 7838), script = function () return menu.buttonShipConfig("purchase") end })
			end
			if canequipships then --Ship on deck equipping
				local dockedships = {}
				if C.IsComponentClass(menu.componentSlot.component, "container") then
					Helper.ffiVLA(dockedships, "UniverseID", C.GetNumDockedShips, C.GetDockedShips, menu.componentSlot.component, "player")
				end
				
				local active = false
				for _, ship in ipairs(dockedships) do
					if C.CanContainerEquipShip(menu.componentSlot.component, ship) then
						active = true
						break
					end
				end
				local mouseovertext
				if not active then
					if #dockedships > 0 then
						-- if the option is inactive, all ships are either capships or not, so only check the first one
						if C.IsComponentClass(dockedships[1], "ship_l") or C.IsComponentClass(dockedships[1], "ship_xl") then
							mouseovertext = ReadText(1026, 7807)
						else
							mouseovertext = ReadText(1026, 7806)
						end
					else
						mouseovertext = ReadText(1026, 7808)
					end
				end
				menu.insertInteractionContent("main", { type = actiontype, text = ReadText(1001, 7841), script = function () return menu.buttonShipConfig("upgrade") end, active = active, mouseOverText = mouseovertext })
			end
			
			if canequipships and #menu.selectedplayerships > 0 and menu.possibleorders["Repair"] then --Ship on deck equipping. ToDo: Refactor this.
				local active = false
				for _, ship in ipairs(menu.selectedplayerships) do
					if C.CanContainerEquipShip(menu.componentSlot.component, ship) then
						active = true
						break
					end
				end
				local mouseovertext
				if (not active) and (#menu.selectedplayerships > 0) then
					-- if the option is inactive, all ships are either capships or not, so only check the first one
					if C.IsComponentClass(menu.selectedplayerships[1], "ship_l") or C.IsComponentClass(menu.selectedplayerships[1], "ship_xl") then
						mouseovertext = ReadText(1026, 7805)
					else
						mouseovertext = ReadText(1026, 7804)
					end
				end
				menu.insertInteractionContent("selected_orders", { type = actiontype, text = ReadText(1001, 7826), script = function () return menu.buttonUpgrade(true) end, active = active, mouseOverText = mouseovertext })
			end
		end

		if menu.componentMissions and (type(menu.componentMissions) == "table") then
			for _, missionid in ipairs(menu.componentMissions) do
				local missiondetails = C.GetMissionIDDetails(missionid)
				if ffi.string(missiondetails.mainType) ~= "guidance" then
					menu.insertInteractionContent("main", { text = ReadText(1001, 7850) .. " (" .. ffi.string(missiondetails.missionName) .. ")", script = function () return menu.buttonMissionShow(missionid) end, active = true })
				end
			end
		end
	end

	if menu.componentOrder then
		-- orders (not action based at all)
		menu.insertInteractionContent("order", { type = "removethisorder", text = ReadText(1001, 7831), script = menu.buttonRemoveOrder, active = C.RemoveOrder(menu.componentSlot.component, menu.componentOrder.queueidx, false, true) })
		-- Remove all orders
		local hasremoveableorders = false
		local numorders = C.GetNumOrders(menu.componentSlot.component)
		local currentorders = ffi.new("Order[?]", numorders)
		numorders = C.GetOrders(currentorders, numorders, menu.componentSlot.component)
		for i = numorders, 1, -1 do
			local isdocked, isdocking = GetComponentData(ConvertStringTo64Bit(tostring(menu.componentSlot.component)), "isdocked", "isdocking")
			if (i == 1) and ((ffi.string(currentorders[0].orderdef) == "DockAndWait") and (isdocked or isdocking)) then
				-- do nothing - removing the dock order would create an undock order ... rather have the ship stay put [Nick]
			else
				if C.RemoveOrder(menu.componentSlot.component, i, false, true) then
					hasremoveableorders = true
					break
				end
			end
		end
		menu.insertInteractionContent("order", { type = "removeallorders", text = ReadText(1001, 7832), script = menu.buttonRemoveAllOrders, active = hasremoveableorders })
	elseif menu.mission then
		local missiondetails = C.GetMissionIDDetails(menu.mission)
		if ffi.string(missiondetails.mainType) == "guidance" then
			menu.insertInteractionContent("guidance", { text = ReadText(1001, 3243), script = menu.buttonEndGuidance, active = true })
		else
			local active = menu.mission == C.GetActiveMissionID()
			local buf = {}
			Helper.ffiVLA(buf, "MissionID", C.GetNumMissionThreadSubMissions, C.GetMissionThreadSubMissions, menu.mission)
			for _, submission in ipairs(buf) do
				local submissionEntry = menu.getMissionIDInfoHelper(submission)
				if submissionEntry.active then
					active = true
				end
			end

			menu.insertInteractionContent("guidance", { text = active and ReadText(1001, 3413) or ReadText(1001, 3406), script = active and menu.buttonMissionSetInactive or (function () return menu.buttonMissionSetActive(menu.mission) end), active = true })

			local active = missiondetails.abortable
			if missiondetails.threadMissionID ~= 0 then
				local details = C.GetMissionIDDetails(missiondetails.threadMissionID)
				active = ffi.string(details.threadType) ~= "sequential"
			end

			menu.insertInteractionContent("guidance", { text = ReadText(1001, 3407), script = function () return menu.buttonMissionAbort(menu.mission) end, active = active })
			--menu.insertInteractionContent("guidance", { text = ReadText(1001, 7850), script = function () return menu.buttonMissionShow(menu.mission) end, active = true })
			local missionid = menu.mission
			if missiondetails.threadMissionID ~= 0 then
				missionid = ConvertStringTo64Bit(tostring(missiondetails.threadMissionID))
			end
			menu.insertInteractionContent("guidance", { text = ReadText(1001, 3326), script = function () return menu.buttonMissionBriefing(missionid, false) end, active = true })
		end
	elseif menu.missionoffer then
		menu.insertInteractionContent("guidance", { text = ReadText(1001, 57), script = function () return menu.buttonMissionAccept(menu.missionoffer) end, active = true })
		menu.insertInteractionContent("guidance", { text = ReadText(1001, 3326), script = function () return menu.buttonMissionBriefing(menu.missionoffer, true) end, active = true })
	elseif menu.construction then
		-- construction (not action based at all)
		menu.insertInteractionContent("main", { text = ReadText(1001, 7853), script = menu.buttonCancelConstruction, active = ((not menu.construction.inprogress) or C.CanCancelConstruction(menu.componentSlot.component, menu.construction.id)) and (menu.construction.factionid == "player") })
	end
end

function menu.getHolomapColors()
	local productioncolor, buildcolor, storagecolor, radarcolor, dronedockcolor, efficiencycolor, defencecolor, playercolor, friendcolor, enemycolor, missioncolor, currentplayershipcolor, visitorcolor, lowalertcolor, mediumalertcolor, highalertcolor = GetHoloMapColors()
	menu.holomapcolor = { productioncolor = productioncolor, buildcolor = buildcolor, storagecolor = storagecolor, radarcolor = radarcolor, dronedockcolor = dronedockcolor, efficiencycolor = efficiencycolor, defencecolor = defencecolor, playercolor = playercolor, friendcolor = friendcolor, enemycolor = enemycolor, missioncolor = missioncolor, currentplayershipcolor = currentplayershipcolor, visitorcolor = visitorcolor, lowalertcolor = lowalertcolor, mediumalertcolor = mediumalertcolor, highalertcolor = highalertcolor }
end

function menu.prepareTexts()
	menu.texts = {}
	menu.colors = {}

	if menu.componentSlot.component ~= 0 then
		local convertedComponent = ConvertStringTo64Bit(tostring(menu.componentSlot.component))
		menu.buildstorage = ConvertIDTo64Bit(GetComponentData(convertedComponent, "buildstorage"))
		local playerObject = C.GetPlayerObjectID()

		local idcode = ""
		if C.IsComponentClass(menu.componentSlot.component, "object") then
			idcode = " (" .. ffi.string(C.GetObjectIDCode(menu.componentSlot.component)) .. ")"
		end
		local sectorprefix = ""
		if C.IsComponentClass(menu.componentSlot.component, "sector") then
			sectorprefix = ReadText(20001, 201) .. ReadText(1001, 120) .. " "
		end
		local gatedestination = ""
		if C.IsComponentClass(menu.componentSlot.component, "gate") then
			local gatedestinationid = GetComponentData(convertedComponent, "destination")
			if gatedestinationid then
				gatedestinationid = ConvertStringTo64Bit(tostring(GetComponentData(gatedestinationid, "sectorid")))
				gatedestination = ReadText(1001, 120) .. " " .. Helper.unlockInfo(GetComponentData(convertedComponent, "isactive") and C.IsInfoUnlockedForPlayer(gatedestinationid, "name"), ffi.string(C.GetComponentName(gatedestinationid)))
			end
			idcode = ""
		end
		local name_unlocked = IsInfoUnlockedForPlayer(convertedComponent, "name")
		menu.texts.targetShortName = Helper.unlockInfo(name_unlocked, ffi.string(C.GetComponentName(menu.componentSlot.component)))
		menu.texts.targetName = sectorprefix .. Helper.unlockInfo(name_unlocked, ffi.string(C.GetComponentName(menu.componentSlot.component)) .. gatedestination .. idcode)
		local isplayerowned, isenemy = GetComponentData(convertedComponent, "isplayerowned", "isenemy")
		menu.colors.target = isplayerowned and ((menu.componentSlot.component == playerObject) and menu.holomapcolor.currentplayershipcolor or menu.holomapcolor.playercolor) or (isenemy and menu.holomapcolor.enemycolor or Helper.color.white)
		menu.texts.targetName = string.format("\027#FF%02x%02x%02x#%s", menu.colors.target.r, menu.colors.target.g, menu.colors.target.b, menu.texts.targetName)
		if C.IsComponentClass(menu.componentSlot.component, "ship") then
			menu.texts.targetBaseName = Helper.unlockInfo(name_unlocked, GetComponentData(convertedComponent, "basename"))
		end

		if menu.construction then
			menu.texts.constructionName = GetMacroData(menu.construction.macro, "name")
		end

		if C.IsComponentClass(menu.componentSlot.component, "controllable") then
			local commander = ConvertIDTo64Bit(GetCommander(convertedComponent))
			if commander and commander ~= 0 then
				menu.texts.commanderShortName = Helper.unlockInfo(IsInfoUnlockedForPlayer(commander, "name"), ffi.string(C.GetComponentName(commander)))
				local idcode = ""
				if C.IsComponentClass(commander, "object") then
					idcode = " (" .. ffi.string(C.GetObjectIDCode(commander)) .. ")"
				end
				menu.texts.commanderName = sectorprefix .. Helper.unlockInfo(IsInfoUnlockedForPlayer(commander, "name"), ffi.string(C.GetComponentName(commander)) .. idcode)
				menu.colors.commander = GetComponentData(commander, "isplayerowned") and ((commander == playerObject) and menu.holomapcolor.currentplayershipcolor or menu.holomapcolor.playercolor) or Helper.color.white
				menu.texts.commanderShortName = string.format("\027#FF%02x%02x%02x#%s", menu.colors.commander.r, menu.colors.commander.g, menu.colors.commander.b, menu.texts.commanderShortName)
				menu.texts.commanderName = string.format("\027#FF%02x%02x%02x#%s", menu.colors.commander.r, menu.colors.commander.g, menu.colors.commander.b, menu.texts.commanderName)
			end
		end
	
		menu.texts.selectedFullNames = ""
		if #menu.selectedplayerships > 0 then
			menu.texts.selectedName = GetComponentData(menu.selectedplayerships[1], "name")
			if #menu.selectedplayerships > 1 then
				menu.texts.selectedName = string.format(ReadText(1001, 7801), #menu.selectedplayerships)
			end
			menu.colors.selected = (menu.selectedplayerships[1] == playerObject) and menu.holomapcolor.currentplayershipcolor or menu.holomapcolor.playercolor
			local first = true
			for _, selectedcomponent in ipairs(menu.selectedplayerships) do
				local isCurrentPlayerObject = (selectedcomponent == playerObject)
				local color = isCurrentPlayerObject and menu.holomapcolor.currentplayershipcolor or menu.holomapcolor.playercolor
				menu.texts.selectedFullNames = menu.texts.selectedFullNames .. (first and "" or "\n") .. string.format("\027#FF%02x%02x%02x#%s", color.r, color.g, color.b, GetComponentData(selectedcomponent, "name") .. " (" .. ffi.string(C.GetObjectIDCode(selectedcomponent)) .. ")" .. (isCurrentPlayerObject and (" (" .. ReadText(1001, 7836) .. ")") or ""))
				first = false
			end
		end
		-- count the interacted object here too
		if isplayerowned and C.IsComponentClass(menu.componentSlot.component, "ship") then
			menu.texts.selectedNameAll = string.format(ReadText(1001, 7801), #menu.selectedplayerships + 1)
			local isCurrentPlayerObject = (convertedComponent == playerObject)
			local color = isCurrentPlayerObject and menu.holomapcolor.currentplayershipcolor or menu.holomapcolor.playercolor
			menu.texts.selectedFullNamesAll = menu.texts.selectedFullNames .. "\n" .. string.format("\027#FF%02x%02x%02x#%s", color.r, color.g, color.b, GetComponentData(convertedComponent, "name") .. " (" .. ffi.string(C.GetObjectIDCode(convertedComponent)) .. ")" .. ((isCurrentPlayerObject) and (" (" .. ReadText(1001, 7836) .. ")") or ""))
		end

		if #menu.selectednpcships > 0 then
			menu.texts.npcName = GetComponentData(menu.selectednpcships[1], "name")
			if #menu.selectednpcships > 1 then
				menu.texts.npcName = string.format(ReadText(1001, 7801), #menu.selectednpcships)
			end
			menu.texts.npcFullNames = ""
			local first = true
			for _, selectedcomponent in ipairs(menu.selectednpcships) do
				local color = Helper.color.white
				menu.texts.npcFullNames = menu.texts.npcFullNames .. (first and "" or "\n") .. string.format("\027#FF%02x%02x%02x#%s", color.r, color.g, color.b, GetComponentData(selectedcomponent, "name") .. " (" .. ffi.string(C.GetObjectIDCode(selectedcomponent)) .. ")")
				first = false
			end
		end

		if menu.buildstorage then
			local name_unlocked = IsInfoUnlockedForPlayer(menu.buildstorage, "name")
			menu.texts.buildstorageName = Helper.unlockInfo(name_unlocked, ffi.string(C.GetComponentName(menu.buildstorage)))
		end
	elseif menu.mission then
		local missiondetails = C.GetMissionIDDetails(menu.mission)
		menu.texts.targetShortName = ffi.string(missiondetails.missionName)
	elseif menu.missionoffer then
		menu.texts.targetShortName = GetMissionOfferDetails(ConvertStringToLuaID(menu.missionoffer))
	end
end

-- update
menu.updateInterval = 0.1

function menu.onUpdate()
	if menu.mode ~= "shipconsole" then
		if (GetControllerInfo() ~= "gamepad") or (C.IsMouseEmulationActive()) then
			local curpos = table.pack(GetLocalMousePosition())
			if curpos[1] and ((curpos[1] < menu.mouseOutBox.x1) or (curpos[1] > menu.mouseOutBox.x2)) then
				menu.onCloseElement("close")
				return
			elseif curpos[2] and ((curpos[2] > menu.mouseOutBox.y1) or (curpos[2] < menu.mouseOutBox.y2)) then
				menu.onCloseElement("close")
				return
			end
		end
	end

	if menu.checkPlayerActivity then
		local playerActivity = GetPlayerActivity()
		if playerActivity ~= menu.currentActivity then
			if (playerActivity == "scan") or (menu.currentActivity == "scan") then
				menu.prepareActions()
				menu.refresh = true
			end
			menu.currentActivity = playerActivity
		end
	end

	local curTime = getElapsedTime()
	if menu.refresh then
		menu.refresh = nil
		menu.lock = curTime
		menu.selectedRows.contentTable = Helper.currentTableRow[menu.contentTable]
		menu.draw()
	end
	if menu.lock and (menu.lock + 0.1 < curTime) then
		menu.lock = nil
	end
end


-- helper hooks

function menu.onButtonOver(uitable, row, col, button)
	if uitable == menu.contentTable then
		local data = menu.rowDataMap[uitable][row]
		if not menu.lock then
			menu.handleSubSectionOption(data)
		end
	end
end

function menu.onCloseElement(dueToClose, allowAutoMenu)
	if dueToClose == "back" then
		if menu.subsection then
			menu.subsection = nil
			menu.refresh = true
			return
		end
	end

	if menu.shown then
		if menu.interactMenuID then
			C.NotifyInteractMenuHidden(menu.interactMenuID, true)
		end
		Helper.closeMenu(menu, dueToClose, allowAutoMenu, false)
		menu.cleanup()
	else
		Helper.resetUpdateHandler()
		Helper.clearFrame(menu, config.layer)
		Helper.interactMenuCallbacks.onTableMouseOut(menu.currentOverTable)
		Helper.resetInteractMenuCallbacks()
		menu.cleanup()
	end
end

function menu.onRowChanged()
	menu.lock = getElapsedTime()
end

function menu.onTableMouseOut(uitable, row)
	if not menu.shown then
		Helper.interactMenuCallbacks.onTableMouseOut(uitable, row)
	end
end

function menu.onTableMouseOver(uitable, row)
	if not menu.shown then
		menu.currentOverTable = uitable
		Helper.interactMenuCallbacks.onTableMouseOver(uitable, row)
	end
end

function menu.viewCreated(layer, ...)
	menu.contentTable = ...
end

init()
