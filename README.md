# Mobile Ship Production #

Enables the hidden resupplier ships and restores their intended functionality as mobile equipment docks.
Players can now buy or build their own resupplier ships at any Argon, Teladi, Paranid or Player (provided they have the appropriate blueprint) shipyard.
These resupplier ships work as mobile equipment docks and/or wharfs and players can use them to equip their ships or produce new ones as it they were stations, with the benefit of them being fully functional ships.
Vanguard variants of the resupplier ships work as equipment docks (they can modify and repair ships but produce new ones), while Sentinel variants work as fully functional wharfs capable of building new ships.
Please, do note that, for now, players will be required to supply the required building wares to the ship manually, either by trading with resupplier itself or by using M size freighters.

### Installation ###

1. Download the latest version from the [downloads page](https://bitbucket.org/aftokinito/x4-mobileshipproduction/downloads/mobileshipproduction.zip) 
2. Extract the contents of the ZIP file into the extensions folder of your game's ROOT  
(example: ```C:\Program Files (x86)\Steam\steamapps\common\X4 Foundations\extensions\```)
3. Make sure you install all the required dependencies, if needed.

### Dependencies ###

* [[MOD] Player Owned Ship Production](https://forum.egosoft.com/viewtopic.php?&t=403662)